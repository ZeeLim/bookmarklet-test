/**
 * Module dependencies.
 */

var express = require('express'),
    http    = require('http'),
    https   = require('https'),
    request = require('request'),
    cheerio = require('cheerio');

var app = express();

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.logger('dev'));
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

app.configure('development', function(){
  console.log('in the development configuration');
  app.set('port', 10011);
  app.set('urlPath', 'beta-Bobby');
  app.use('/beta-Bobby',express.static(__dirname + '/public'));
  app.use(express.errorHandler());

});

app.configure('production', function (){
  console.log('in the production configuration');
  app.set('port', 10012);
  app.set('urlPath', 'bookmark');
  app.use('/bookmark', express.static(__dirname + '/public'));
  app.use(express.errorHandler());
});

app.get('/' + app.get('urlPath'), function(req, res){
  res.render('show_bookmarklet',{
    urlPath : app.get('urlPath')
  });
});

app.get('/' + app.get('urlPath') + '/bookmarklet', function(req, res){
  var url     = req.query.url,
      title   = req.query.title,
      domain  = req.query.domain,
      version = req.query.version,
      domain  = url.substring(0,(url.indexOf(".com") + 4)),

      og_url       = null,
      og_title     = null,
      og_image     = null,
      og_locale    = null,
      og_site_name   = null,
      og_description = null;

  var imgs = new String(); 
   
  request(url, function (error, response, html) {
    if (!error && (response.statusCode == 200)) {

      if (url.indexOf(".pdf") != -1){
        console.log(url);
      }
      else if(url.indexOf(".jpg") != -1 || url.indexOf("png") != -1 || url.indexOf(".gif") != -1 || url.indexOf(".img") != -1){
        og_image = url;
      }
      else{
        console.log("hello");
        var $ = cheerio.load(html);
      
        if ( $('meta').length ) {

          var count = 0;

          $('meta').each(function(i, element){
            var meta = $(this);

            switch(meta.attr('property')) {

              case 'og:url' :
              og_url = meta.attr('content');
              console.log(og_url);
              break;

              case 'og:title' :
              og_title = meta.attr('content');
              console.log(og_title);
              break;

              case 'og:image' :
              if(count == 0){
                imgs = meta.attr('content');
                og_image = meta.attr('content');
              }
              else{
                imgs = imgs.concat("," + meta.attr('content'));
                og_image = "";
              }
              console.log(og_image);
              count++;
              break;

              case 'og:locale' :
              og_locale = meta.attr('content');
              console.log(og_locale);
              break;

              case 'og:site_name':
              og_site_name = meta.attr('content');
              console.log(og_site_name);
              break;

              case 'og:description':
              og_description = meta.attr('content');
              console.log(og_description);
              break;  
            }
          
            if (og_description == null) {
              if(meta.attr('name')=="description")
                og_description = meta.attr('content');
            }
          }); 
        } 

        //new code to put the images scraped in a string
        if (og_image == null){
          if($('img').length){
            $('img').each(function(i, element){
              var img = $(this);
              if(imgs == "" && typeof(img.attr('src'))!="undefined"){
                if(img.attr('src').charAt(0) === '/')
                  imgs = domain + img.attr('src');
                else imgs = img.attr('src');
              }
              else if(typeof(img.attr('src'))!="undefined"){
                if(img.attr('src').charAt(0) === '/')
                  imgs = imgs.concat("," + domain + img.attr('src'));
                else imgs = imgs.concat("," + img.attr('src'));
              } 
            });
          }
        }
      }console.log("see here" + imgs);
    }

    var redirectingTo = '/' + app.get('urlPath') + '/add?version='+encodeURIComponent(version)+'&url='+encodeURIComponent(url); 
    redirectingTo += '&title='+encodeURIComponent(title)+'&og_title='+encodeURIComponent(og_title)+'&og_url='+encodeURIComponent(og_url);
    redirectingTo += '&og_locale='+encodeURIComponent(og_locale)+'&og_site_name='+encodeURIComponent(og_site_name);
    redirectingTo += '&og_description='+encodeURIComponent(og_description)+'&og_image='+encodeURIComponent(og_image)+'&imgs='+encodeURIComponent(imgs)+'&';
//redirects the bookmarklet applet to show the curate and share buttons, with the meta attributes already in the URL for access in the next get function
    res.redirect(redirectingTo);

  });
});

app.get('/' + app.get('urlPath') + '/add', function(req, res){
  /*Detect and escape special character which will cause syntax error 
    for "description" only */
  var tmp = req.query.og_description,
      description;
  if ( tmp == null ) {
    description = null;
  } else {
    var escapeChar = [];
    //detect the location of special character and push into an array
    for ( var i=0; i < tmp.length; i++ ) {
      switch( tmp.charAt(i) ) {
        case "\n": escapeChar.push(i);
          continue;
        case "\r": escapeChar.push(i);
          continue;
      }
    }
    //escape all the special character at once
    if ( escapeChar.length ) {
      for ( var j=0; j < escapeChar.length; j++ ) {
        tmp = tmp.slice(0, escapeChar[j] + j) + "\\" + tmp.slice(escapeChar[j] + j);
        description = tmp;
      }
    } else {
      description = tmp;
    }
  }

  res.render('app', {
    urlPath        : app.get('urlPath'),
    version        : req.query.version,
    url            : req.query.url,
    title          : req.query.title,
    og_url         : req.query.og_url,
    og_title       : req.query.og_title,
    og_image       : req.query.og_image,
    og_locale      : req.query.og_locale,
    og_site_name   : req.query.og_site_name,
    og_description : description,
    images         : req.query.imgs
  });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
