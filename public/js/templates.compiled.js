this["NewzSocial"] = this["NewzSocial"] || {};
this["NewzSocial"]["Template"] = this["NewzSocial"]["Template"] || {};

this["NewzSocial"]["Template"]["curate"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n            <option value='"
    + escapeExpression(((stack1 = depth0[0]),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "'>"
    + escapeExpression(((stack1 = depth0[1]),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</option>\r\n\r\n          ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n            <img class=\"img-polaroid\" src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" style=\"width:222px;height:150px\" />\r\n\r\n            ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n                ";
  stack2 = helpers['if'].call(depth0, ((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.imgs)),stack1 == null || stack1 === false ? stack1 : stack1[1]), {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n            ";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "    \r\n\r\n                        <div class='imageViewer'>    \r\n                            <div id=image>\r\n                                <img id=\"myImage\" src=\""
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.imgs)),stack1 == null || stack1 === false ? stack1 : stack1[0])),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" style=\"width:222px;height:150px\" />\r\n                            </div>\r\n                            \r\n                            <button class=\"leftarrow\" onclick=\"ChangeImageSrcPrev()\"><</button>\r\n                            <button class=\"rightarrow\" onclick=\"ChangeImageSrcNext()\">></button>\r\n                        </div>\r\n                ";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                \r\n                    ";
  stack1 = helpers['if'].call(depth0, depth0.pdfImg, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "        \r\n\r\n                ";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                                            \r\n                        <img id=\"PDF\" src=\"";
  if (stack1 = helpers.pdfImg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.pdfImg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"width:222px;height:210px\" />\r\n                                \r\n                    ";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.PDF), {hash:{},inverse:self.program(21, program21, data),fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                ";
  return buffer;
  }
function program12(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        <div class=\"article-info\">\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title), {hash:{},inverse:self.program(15, program15, data),fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        <div class='info1'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                        <div class='info2'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content), {hash:{},inverse:self.program(19, program19, data),fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                            <input id=\"image-url\" type=\"text\" placeholder=\"Add image URL…\" />\r\n                            <button id=\"preview\" onclick=\"previewImg()\">Preview</button>\r\n\r\n                        </div>\r\n\r\n                    ";
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                            <h5 id=\"title\" class=\"article-title\"><a target=\"_blank\" href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a></h5>\r\n                        ";
  return buffer;
  }

function program15(depth0,data) {
  
  
  return "\r\n                            <input id=\"title-of-pdf\" type=\"text\" placeholder=\"Add a title…\" />\r\n                        ";
  }

function program17(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                            <p class=\"article-abstract\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                        ";
  return buffer;
  }

function program19(depth0,data) {
  
  
  return "\r\n                            <textarea id=\"abstract\" placeholder=\"About the Article…\" rows=\"3\" cols=\"20\"></textarea>\r\n                        ";
  }

function program21(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        <div class=\"article-info\">\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title), {hash:{},inverse:self.program(15, program15, data),fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        <div class='info1'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                        <div class='info2'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content), {hash:{},inverse:self.program(19, program19, data),fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n                        </div>\r\n                    ";
  return buffer;
  }

function program23(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                    <h5 class=\"article-title\"><a target=\"_blank\" href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a></h5>\r\n                    <p class=\"article-publisher\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                    <p class=\"article-abstract\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                ";
  return buffer;
  }

  buffer += "<section class=\"curate-template\">\r\n\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheets/imgviewer.css\">\r\n\r\n    <div class=\"container-header\">\r\n\r\n        <div class=\"page-information\">\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\">\r\n            <h4 class=\"page-title\">Curate Article</h4>\r\n        </div>\r\n      \r\n        <div class=\"user-information-dropdown-container dropdown\">\r\n\r\n            <a class=\"user-information dropdown-toggle\" data-toggle=\"dropdown\">\r\n                <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.img)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\r\n            </a>\r\n\r\n            <ul class=\"dropdown-menu\" >\r\n                <li><span class=\"user-profile\" style=\"cursor:default;\">Hello <b>"
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.userName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</b></span></li>\r\n                <li class=\"divider\"></li>\r\n                <li><a class=\"clickable\" target=\"_blank\" href=\"http://business.newzsocial.com/newzlink/mychannels\">View All My Channels</a></li> \r\n                <li><a class=\"clickable\" onclick=\"signout()\">Sign Out</a></li>\r\n            </ul>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div class=\"container-body\">\r\n    \r\n        <div class=\"curate-comment-section\">\r\n            \r\n            <div id=\"longCommentTab\" class=\"curate-comment-tabs active\">Comment</div>\r\n            <div id=\"shortCommentTab\" class=\"curate-comment-tabs\">Short Version</div>\r\n            <textarea id=\"curateLongComment\" class=\"curate-comment-box\" placeholder=\"Add a comment…\" ></textarea>\r\n            <textarea id=\"curateShortComment\" class=\"curate-comment-box hide\" data-state=\"firstTime\" onkeydown=\"\" maxlength=\"115\" placeholder=\"Add a comment…\" ></textarea>\r\n            <span id=\"shortCommentLimit\" class=\"short-comment-count hide\"></span>\r\n\r\n        </div>\r\n\r\n        <span class=\"curate-into-text\">Curate into:<span>\r\n\r\n        <select id='channels-select' multiple=\"multiple\" onchange=\"autoResize(680)\">\r\n\r\n          ";
  stack2 = helpers.each.call(depth0, depth0.channels, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n        </select>\r\n        \r\n        <div class=\"article-information\">\r\n\r\n            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.image), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n            <div class=\"article-body\">\r\n                ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.isPDForIMG), {hash:{},inverse:self.program(23, program23, data),fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"container-footer\">\r\n      <button id=\"back-btn\" class=\"btn\" onclick=\"back()\">BACK</button>\r\n      <a href=\"#\" class=\"btn\" onclick=\"closeBookmarklet()\">CLOSE</a>\r\n      <!-- <a href=\"#\" class=\"btn\" onclick=\"curateArticle(true,'";
  if (stack2 = helpers.article_id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.article_id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "')\">CLIP AND CURATE</a> -->\r\n      <a href=\"#\" class=\"btn\" onclick=\"curateArticle(false,'";
  if (stack2 = helpers.article_id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.article_id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "')\">CURATE</a>\r\n    </div>\r\n\r\n</section>";
  return buffer;
  });

this["NewzSocial"]["Template"]["message"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, self=this;

function program1(depth0,data) {
  
  
  return "\r\n\r\n    <div class=\"container-body\">\r\n\r\n        <div class=\"reset-password-container\">\r\n            \r\n            <center>\r\n                <h5>Fill in your email below and we will send out a new temporary password to you</h5>\r\n            </center>\r\n\r\n            <div class=\"signup-input-container\">\r\n                <input id=\"reset-userEmail\" type=\"text\" placeholder=\"Email Address\" onkeypress=\"enterKey(event, 'resetpassword')\"><br>\r\n                <button class=\"reset-password-btn\" onclick=\"resetPassword()\">Send Password</button>\r\n            </div>\r\n\r\n            <hr>\r\n\r\n            <div class=\"switch-page-link\">\r\n                <a href=\"javascript:renderNewzSocialSignIn();\">Sign In To NewzSocial</a>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n    ";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n    <div class='container-body'>\r\n        \r\n        ";
  stack1 = helpers['if'].call(depth0, depth0.transition, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n        ";
  stack1 = helpers['if'].call(depth0, depth0.searching, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n        ";
  stack1 = helpers['if'].call(depth0, depth0.sharing, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n    </div>\r\n\r\n    ";
  return buffer;
  }
function program4(depth0,data) {
  
  
  return "\r\n            <center>\r\n                <h3 id=\"message\" class='message'></h3>\r\n            </center>\r\n        ";
  }

function program6(depth0,data) {
  
  
  return "\r\n            <center>\r\n                <div class=\"message-container\">\r\n                    <img src=\"images/loading.gif\">\r\n                    <h3>Finding Article...</h3>\r\n                </div>\r\n            </center>\r\n        ";
  }

function program8(depth0,data) {
  
  
  return "\r\n            <center>\r\n                <div class=\"message-container\">\r\n                    <img src=\"images/loading.gif\">\r\n                    <h3>Sharing Article...</h3>\r\n                </div>\r\n            </center>\r\n        ";
  }

  buffer += "<section class=\"message-template\">\r\n\r\n    <div class='container-header'>\r\n        <center>\r\n        <div class=\"newzsocial-logo-container\">\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\">\r\n        </div>\r\n        </center>\r\n    </div>\r\n\r\n    ";
  stack1 = helpers['if'].call(depth0, depth0.reset, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n</section>";
  return buffer;
  });

this["NewzSocial"]["Template"]["newzsocial_actions"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<section class=\"newzsocial-action-template\">\r\n\r\n    <div class=\"container-header header-shadow\">\r\n\r\n        <div class=\"newzsocial-logo-container\">\r\n            <img class=\"newzsocial-logo\" src=\"images/newzsocial.png\" alt=\"newzsocial\">\r\n        </div>\r\n\r\n        <div class=\"user-information-dropdown-container dropdown\">\r\n\r\n            <a class=\"user-information dropdown-toggle\" data-toggle=\"dropdown\">\r\n                <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.img)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\r\n            </a>\r\n\r\n            <ul class=\"dropdown-menu\" >\r\n                <li><span class=\"user-profile\" style=\"cursor:default;\">Hello <b>"
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.userName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</b></span></li>\r\n                <li class=\"divider\"></li>\r\n                <li><a class=\"clickable\" target=\"_blank\" href=\"http://business.newzsocial.com/newzlink/mychannels\">View All My Channels</a></li> \r\n                <li><a class=\"clickable\" onclick=\"signout()\">Sign Out</a></li>\r\n            </ul>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div class=\"container-body\">\r\n\r\n        <center class=\"newzsocial-action\">\r\n            <!-- <button type=\"button\" class=\"btn-ccs btn-clip\" onclick=\"clip('";
  if (stack2 = helpers.article_id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.article_id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "')\"></button> -->\r\n            <button type=\"button\" class=\"btn-ccs btn-curate\" onclick=\"curate('";
  if (stack2 = helpers.article_id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.article_id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "')\"></button>\r\n            <button type=\"button\" class=\"btn-ccs btn-share\" onclick=\"share('default','";
  if (stack2 = helpers.article_id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.article_id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "')\"></button>\r\n        </center>\r\n      \r\n    </div>\r\n\r\n</section>\r\n\r\n";
  return buffer;
  });

this["NewzSocial"]["Template"]["non_verified"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, self=this, functionType="function", escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  
  return "\r\n\r\n        <center>\r\n            <h4>Your NewzSocial account has been created!</h4>\r\n            <h5>Please verify your account through an email that we just sent you!</h5>\r\n        </center>\r\n\r\n    ";
  }

function program3(depth0,data) {
  
  
  return "\r\n\r\n        <center>\r\n            <h4>Your account has not been verified.</h4>\r\n            <h5>Please verify your account through an email that we've sent you.</h5>\r\n        </center>\r\n\r\n    ";
  }

  buffer += "<section class=\"non-verified-template\">\r\n\r\n    <div class=\"container-header\">\r\n        <center>\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\">  \r\n        </center>\r\n    </div>\r\n\r\n    <div class=\"container-body\">\r\n\r\n    ";
  stack1 = helpers['if'].call(depth0, depth0.newAccount, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n        <div class=\"switch-page-link\">\r\n            <button class=\"resend-verification-btn\" onclick=\"resendEmailVerification( '";
  if (stack1 = helpers.userEmail) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.userEmail; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "' )\">Resend Verification Email</button>\r\n        </div>\r\n\r\n        <hr>\r\n\r\n        <div class=\"switch-page-link\">\r\n            <a href=\"javascript:renderNewzSocialSignIn();\">Sign In To NewzSocial</a>\r\n        </div>\r\n\r\n    </div>\r\n\r\n</section>";
  return buffer;
  });

this["NewzSocial"]["Template"]["share"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n    <div class='signup-container'>\r\n\r\n        <div class=\"container-header\">\r\n\r\n            <div class=\"page-information\">\r\n                <img class=\"newzsocial-logo\" src=\"images/newzsocial.png\" alt=\"newzsocial\">\r\n            </div>\r\n            \r\n            <div class=\"user-information-dropdown-container dropdown\">\r\n\r\n                <a class=\"user-information dropdown-toggle\" data-toggle=\"dropdown\">\r\n                    <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.img)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\r\n                </a>\r\n\r\n                <ul class=\"dropdown-menu\" >\r\n                    <li><span class=\"user-profile\" style=\"cursor:default;\">Hello <b>"
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.userName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</b></span></li>\r\n                    <li class=\"divider\"></li>\r\n                    <li><a class=\"clickable\" target=\"_blank\" href=\"http://business.newzsocial.com/newzlink/mychannels\">View All My Channels</a></li> \r\n                    <li><a class=\"clickable\" onclick=\"signout()\">Sign Out</a></li>\r\n                </ul>\r\n\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class='container-body signup-body'>\r\n\r\n            ";
  stack2 = helpers['if'].call(depth0, depth0.email, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n        </div>\r\n      \r\n        <div class='container-footer'>\r\n            <a href=\"#\" class=\"btn\" onclick=\"closeBookmarklet()\">CLOSE</a>\r\n        </div>\r\n      \r\n    </div>\r\n\r\n    ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "\r\n\r\n            <div class=\"signup-email\">\r\n\r\n                    <center><h4>You Do Not Have A NewzSocial Email Profile</h4></center>\r\n\r\n                    <div class=\"signup-input-container\">\r\n                        <!-- Email signup field -->  \r\n                        <input id=\"userName\" type=\"text\" placeholder=\"Username\" onkeypress=\"enterKey(event, 'signupmerge')\"><br>  \r\n                        <input id=\"userEmail\" type=\"text\" placeholder=\"Email Address\" onkeypress=\"enterKey(event, 'signupmerge')\"><br> \r\n                        <input id=\"userPwd\" type=\"password\" placeholder=\"Password\" onkeypress=\"enterKey(event, 'signupmerge')\"><br>\r\n                        <input id=\"userConfirmationPwd\" type=\"password\" placeholder=\"Password Confirmation\" onkeypress=\"enterKey(event, 'signupmerge')\"><br>\r\n                        <button class=\"signup-email-btn\" onclick=\"signUp(true)\">Sign Up Now!</button>\r\n\r\n                    </div>\r\n\r\n                    <div class=\"switch-page-link\">\r\n                        <a href=\"javascript:switchMergingPage(true);\">Already have a NewzSocial account? (Merge)</a>\r\n                    </div>\r\n\r\n            </div>\r\n\r\n            <div class=\"account-merging-container hide\">\r\n                \r\n                <center>\r\n                    <h4>Please Sign In Using Your NewzSocial Email Account Before Merging With Your Social Media Profiles</h4>\r\n                    <h5>You can merge all your Social Media profiles into one account</h5>\r\n                </center>\r\n\r\n                <div class=\"signin-input-container\">\r\n                    <!-- Email signin field -->  \r\n                    <input name=\"userEmail\" id=\"userEmail-signin\" type=\"text\" placeholder=\"Email\" onkeypress=\"enterKey(event, 'merge')\"><br> \r\n                    <input name=\"userPwd\" id=\"userPwd-signin\" type=\"password\" placeholder=\"Password\" onkeypress=\"enterKey(event, 'merge')\"><br>\r\n                    <button class=\"merge-email-btn\" onclick=\"mergeSocialMediaProfiles()\">Merge Now!</button>\r\n\r\n                </div>\r\n\r\n                <div class=\"switch-page-link\">\r\n                    <a href=\"javascript:switchMergingPage(false);\">Create a NewzSocial account</a>\r\n                </div>\r\n\r\n            </div>\r\n\r\n            <div class=\"account-merging-non-verified-container hide\">\r\n\r\n                <center>\r\n                  <h4>Your NewzSocial Account Has Not Been Verified</h4>\r\n                  <h5>Please verify your account through an email that we've sent you!</h5><br>\r\n                </center>\r\n\r\n                <div class=\"switch-page-link\">\r\n                    <button id=\"resendEmail\" class=\"resend-verification-btn\">Resend Verification Email</button>\r\n                </div>\r\n\r\n                <div class=\"switch-page-link\">\r\n                    <a href=\"javascript:switchMergingPage(false);\">Create a NewzSocial account</a>\r\n                </div>\r\n\r\n                <div class=\"switch-page-link\">\r\n                    <a href=\"javascript:switchMergingPage(true);\">Merge another NewzSocial account</a>\r\n                </div>\r\n                \r\n            </div>\r\n\r\n            ";
  }

function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n            \r\n            <div class=\"signup-social\">\r\n\r\n                <center><h4>You Do Not Have A <span id=\"share-pipe-title\">";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span> Profile Enabled For NewzSocial</h4></center>\r\n              \r\n                ";
  stack1 = helpers['if'].call(depth0, depth0.facebook, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n                ";
  stack1 = helpers['if'].call(depth0, depth0.linkedin, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n                ";
  stack1 = helpers['if'].call(depth0, depth0.twitter, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n            </div>\r\n    \r\n            ";
  return buffer;
  }
function program5(depth0,data) {
  
  
  return "\r\n                    <center><button  class=\"social-btn\" onclick=\"NAS.fbSignIn(fbCallBack);return false;\"><span class=\"social-btn-img facebook-icon\"></span>Sign Up With Facebook</button></center>\r\n                ";
  }

function program7(depth0,data) {
  
  
  return "\r\n                    <center><button class=\"social-btn\" onclick=\"NAS.lnSignIn(linkCallBack);return false;\"><span class=\"social-btn-img linkedin-icon\"></span>Sign Up With LinkedIn</button></center>\r\n                ";
  }

function program9(depth0,data) {
  
  
  return "\r\n                    <center><button class=\"social-btn\" onclick=\"NAS.twtSignIn(twtCallBack);return false;\"><span class=\"social-btn-img twitter-icon\"></span>Sign Up With Twitter</button></center>\r\n                ";
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n    <div class=\"share-container\">\r\n        \r\n        <div class=\"container-header\">\r\n\r\n            <div class=\"page-information\">\r\n                <img class=\"newzsocial-logo\" src=\"images/newzsocial.png\" alt=\"newzsocial\">\r\n                <h4 class=\"page-title\">Share Article <span id=\"share-pipe-title\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span></h4>\r\n            </div>\r\n\r\n            <div class=\"user-information-dropdown-container dropdown\">\r\n\r\n                <a class=\"user-information dropdown-toggle\" data-toggle=\"dropdown\">\r\n                    <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.img)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\r\n                </a>\r\n\r\n                <ul class=\"dropdown-menu\" >\r\n                    <li><span class=\"user-profile\" style=\"cursor:default;\">Hello <b>"
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.userName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</b></span></li>\r\n                    <li class=\"divider\"></li>\r\n                    <li><a class=\"clickable\" target=\"_blank\" href=\"http://business.newzsocial.com/newzlink/mychannels\">View All My Channels</a></li> \r\n                    <li><a class=\"clickable\" onclick=\"signout()\">Sign Out</a></li>\r\n                </ul>\r\n\r\n            </div>\r\n      \r\n        </div>\r\n        \r\n        ";
  stack2 = helpers['if'].call(depth0, depth0.isEmail, {hash:{},inverse:self.program(41, program41, data),fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n        <div class=\"container-footer\">\r\n\r\n            <button id=\"back-btn\" class=\"btn\" onclick=\"back()\">BACK</button>\r\n            <button class=\"btn\" onclick=\"closeBookmarklet()\">CLOSE</button>\r\n            <button id=\"shareButton\" class=\"btn hide\" onclick=\"shareArticle('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\">SHARE</button>\r\n            <button id=\"shareButton-twt\" class=\"btn hide\" onclick=\"shareArticle('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\">TWEET</button>\r\n            <button id=\"shareButton-email\" class=\"btn hide\" onclick=\"shareArticle('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\">SEND</button>\r\n            <button id=\"pipeButton\" class=\"btn hide\" onclick=\"pipeArticle()\">SHARE</button>\r\n        \r\n        </div>\r\n    \r\n    </div>\r\n\r\n    ";
  return buffer;
  }
function program12(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n        <div class=\"container-body\">\r\n\r\n            <!-- ";
  stack1 = helpers['if'].call(depth0, depth0.isVerified, {hash:{},inverse:self.program(15, program15, data),fn:self.program(13, program13, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " -->\r\n\r\n        </div>\r\n\r\n        ";
  return buffer;
  }
function program13(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " -->\r\n\r\n          <!--   <div class=\"account-merging-non-verified-container hide\">\r\n\r\n                <center>\r\n                  <h4>Your NewzSocial Account Has Not Been Verified</h4>\r\n                  <h5>Please verify your account through an email that we've sent you!</h5><br>\r\n                </center>\r\n\r\n                <div class=\"switch-page-link\">\r\n                    <button id=\"resendEmail\" class=\"resend-verification-btn\" onclick=\"resendEmailVerification("
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.displayEmail)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ")\">Resend Verification Email</button>\r\n                </div>\r\n                \r\n            </div> -->\r\n\r\n            <!-- ";
  return buffer;
  }

function program15(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += " -->\r\n\r\n            <div>\r\n                <p>From: "
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.displayEmail)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                <!-- <p>From: ihelp@newzsocial.com </p> -->\r\n                <input type=\"text\" id=\"emailInput\" onchange=\"autoResize(680)\" placeholder=\"Please enter email\">\r\n\r\n                <div class=\"emailme-checkbox\">\r\n                    <label class=\"checkbox\">\r\n                        <input type=\"checkbox\" id=\"emailMe\"> Email me a copy too!\r\n                    </label> \r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"email-editor-container\">\r\n\r\n                <input id=\"emailSubject\" type=\"text\"/>\r\n\r\n                <textarea id=\"email-editor\">\r\n\r\n                    <div style='padding: 8px 0;'>\r\n                    &quotI am sharing an interesting article, <i><b> "
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " </b></i> and thought you might enjoy reading it.&quot - "
    + escapeExpression(((stack1 = ((stack1 = depth0.display),stack1 == null || stack1 === false ? stack1 : stack1.userName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\r\n                    </div>      \r\n\r\n                    <div style='padding-top:1em; border-top:1px solid #6D6B58;'></div>\r\n\r\n                </textarea>\r\n                \r\n                    <div id=\"imageDisplay\">\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.image), {hash:{},inverse:self.program(18, program18, data),fn:self.program(16, program16, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n                    </div>\r\n\r\n                    <div class=\"article-body\">\r\n                    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.isPDForIMG), {hash:{},inverse:self.program(39, program39, data),fn:self.program(24, program24, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n            \r\n            </div>\r\n\r\n            <!-- ";
  return buffer;
  }
function program16(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n                            <div style='max-width:200px; float:left; margin-right:1em;'>\r\n                            <img class=\"article-image\" src='"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "' style=\"width:222px;height:150px\" />\r\n                            </div>\r\n\r\n                        ";
  return buffer;
  }

function program18(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.imgs)),stack1 == null || stack1 === false ? stack1 : stack1[1]), {hash:{},inverse:self.program(21, program21, data),fn:self.program(19, program19, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        ";
  return buffer;
  }
function program19(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n                            <div class='imageViewer'>    \r\n                                <div id=image>\r\n                                <img id=\"myImage\" src=\""
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.imgs)),stack1 == null || stack1 === false ? stack1 : stack1[0])),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" style=\"width:222px;height:190px\" />\r\n                                </div>\r\n\r\n                                <button class=\"leftarrow\" onclick=\"ChangeImageSrcPrev()\"><</button>\r\n                                <button class=\"rightarrow\" onclick=\"ChangeImageSrcNext()\">></button>\r\n                            </div>\r\n\r\n                            ";
  return buffer;
  }

function program21(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                ";
  stack1 = helpers['if'].call(depth0, depth0.pdfImg, {hash:{},inverse:self.noop,fn:self.program(22, program22, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n\r\n                            ";
  return buffer;
  }
function program22(depth0,data) {
  
  var buffer = "", stack1;
  buffer += " \r\n                        \r\n                                <img id=\"PDF\" src=\"";
  if (stack1 = helpers.pdfImg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.pdfImg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"width:222px;height:210px\" />\r\n                            \r\n                                ";
  return buffer;
  }

function program24(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.PDF), {hash:{},inverse:self.program(34, program34, data),fn:self.program(25, program25, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                ";
  return buffer;
  }
function program25(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        <div class=\"article-info\">\r\n\r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title), {hash:{},inverse:self.program(28, program28, data),fn:self.program(26, program26, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                            <div class='info1-email'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                            <div class='info2-email'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                        \r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content), {hash:{},inverse:self.program(32, program32, data),fn:self.program(30, program30, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                                <input id=\"image-url\" class=\"email-share-params\" type=\"text\" placeholder=\"Add image URL…\" />\r\n                                <button id=\"preview\" class=\"btn-email-param\" onclick=\"previewImg()\">Preview</button>\r\n                        </div>\r\n\r\n                    ";
  return buffer;
  }
function program26(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                <h5 id=\"title\" class=\"article-title\"><a target=\"_blank\" href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a></h5>\r\n                            ";
  return buffer;
  }

function program28(depth0,data) {
  
  
  return "\r\n                                <input id=\"title-of-pdf\" class=\"email-share-params\" type=\"text\" placeholder=\"Add a title…\" />\r\n                            ";
  }

function program30(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                <p class=\"article-abstract-email\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                            ";
  return buffer;
  }

function program32(depth0,data) {
  
  
  return "\r\n                                <textarea id=\"abstract\" class=\"email-share-params\" placeholder=\"About the Article…\" rows=\"3\" cols=\"20\"></textarea>\r\n                            ";
  }

function program34(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        <div class=\"article-info\">\r\n                           \r\n                            <input id=\"title-of-pdf\" class=\"img-params\" type=\"text\" placeholder=\"Add a title…\" />\r\n                            \r\n\r\n                            <div class='info1-img'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                            <div class='info2-img'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n\r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content), {hash:{},inverse:self.program(37, program37, data),fn:self.program(35, program35, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        </div>\r\n\r\n                    ";
  return buffer;
  }
function program35(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                <p class=\"article-abstract-img\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                            ";
  return buffer;
  }

function program37(depth0,data) {
  
  
  return "\r\n                                <textarea id=\"abstract\" class=\"img-params\" placeholder=\"About the Article…\" rows=\"3\" cols=\"20\"></textarea>\r\n                            ";
  }

function program39(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                     \r\n                        <div class=\"article-info\">   \r\n                            <h5 id=\"title\" class=\"article-title\"><a target=\"_blank\" href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a></h5>\r\n\r\n                            <div class='info1'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                            <div class='info2'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                    \r\n                            <div style='clear: right;'></div>\r\n\r\n                            <div id=\"content\" class=\"article-abstract\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                        </div>\r\n\r\n                ";
  return buffer;
  }

function program41(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n        <div class=\"container-body\">\r\n\r\n            ";
  stack1 = helpers['if'].call(depth0, depth0.shareTypes, {hash:{},inverse:self.noop,fn:self.program(42, program42, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "  \r\n\r\n            <div class=\"commentbox-container\">\r\n\r\n                <img class=\"img-polaroid\" src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.profile),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\r\n                <textarea id=\"share-user-comment\" placeholder=\"Add a comment…\" onkeyup=\"countWords('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\"></textarea>\r\n                <span id=\"share-comment-count\" class=\"twitter-counter-limit\"></span>\r\n\r\n            </div>\r\n\r\n            <div class=\"article-information\">\r\n\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.image), {hash:{},inverse:self.program(51, program51, data),fn:self.program(49, program49, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        <div class=\"article-body\">\r\n\r\n                ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.isPDForIMG), {hash:{},inverse:self.program(74, program74, data),fn:self.program(57, program57, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n                </div>\r\n            \r\n            </div>\r\n        \r\n        </div>  \r\n\r\n            \r\n        ";
  return buffer;
  }
function program42(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n                \r\n            <div class=\"share-options\">\r\n\r\n                <select id=\"share-type-select\" class=\"share-type\" onchange=\"changePlatformType('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\">\r\n                    \r\n                    ";
  stack2 = helpers.each.call(depth0, depth0.shareTypes, {hash:{},inverse:self.noop,fn:self.program(43, program43, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n                \r\n                </select>  \r\n                \r\n                <select id='share-group-select' class=\"share-group hide\" onchange=\"checkOaData('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\" placeholder=\"- Please Select a Group -\">\r\n\r\n                    <option></option> \r\n\r\n                    ";
  stack2 = helpers.each.call(depth0, depth0.groups, {hash:{},inverse:self.noop,fn:self.program(45, program45, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n                    \r\n                </select>\r\n            \r\n                <select id='share-page-select' class=\"share-page hide\" onchange=\"checkOaData('"
    + escapeExpression(((stack1 = ((stack1 = depth0.sharePipe),stack1 == null || stack1 === false ? stack1 : stack1.pipe)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "')\" placeholder=\"- Please Select a Page -\">\r\n\r\n                    <option></option>\r\n\r\n                    ";
  stack2 = helpers.each.call(depth0, depth0.pages, {hash:{},inverse:self.noop,fn:self.program(47, program47, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                </select>\r\n\r\n            </div>\r\n\r\n            ";
  return buffer;
  }
function program43(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                    \r\n                    <option value=\"";
  if (stack1 = helpers.type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.type; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</option>\r\n                    \r\n                    ";
  return buffer;
  }

function program45(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n                    <option value='";
  if (stack1 = helpers.oaGroupId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.oaGroupId; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "'>";
  if (stack1 = helpers.oaGroupName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.oaGroupName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</option>\r\n\r\n                    ";
  return buffer;
  }

function program47(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n                    <option value='";
  if (stack1 = helpers.oaPageId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.oaPageId; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "'>";
  if (stack1 = helpers.oaPageName) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.oaPageName; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</option>\r\n\r\n                    ";
  return buffer;
  }

function program49(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n                        <img id=\"image\" class=\"img-polaroid\" src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" onerror=\"errorFunc()\" style=\"width:222px;height:150px\" />\r\n\r\n                        ";
  return buffer;
  }

function program51(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n                        \r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.imgs)),stack1 == null || stack1 === false ? stack1 : stack1[1]), {hash:{},inverse:self.program(54, program54, data),fn:self.program(52, program52, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        ";
  return buffer;
  }
function program52(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                            <div class='imageViewer'>    \r\n                                <div id='image'>\r\n                                    <img id=\"myImage\" src=\""
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.imgs)),stack1 == null || stack1 === false ? stack1 : stack1[0])),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" style=\"width:222px;height:190px\" onerror=\"errorFunc()\" />\r\n                                </div>\r\n                            \r\n                                <button class=\"leftarrow\" onclick=\"ChangeImageSrcPrev()\"><</button>\r\n                                <button class=\"rightarrow\" onclick=\"ChangeImageSrcNext()\">></button>\r\n                            </div>\r\n\r\n                            ";
  return buffer;
  }

function program54(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                ";
  stack1 = helpers['if'].call(depth0, depth0.pdfImg, {hash:{},inverse:self.noop,fn:self.program(55, program55, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n                            ";
  return buffer;
  }
function program55(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                    \r\n                                <img id=\"PDF\" src=\"";
  if (stack1 = helpers.pdfImg) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.pdfImg; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" style=\"width:222px;height:210px\" />\r\n\r\n                                ";
  return buffer;
  }

function program57(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.PDF), {hash:{},inverse:self.program(65, program65, data),fn:self.program(58, program58, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                           \r\n\r\n                ";
  return buffer;
  }
function program58(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        <div class=\"article-info\">\r\n                        \r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title), {hash:{},inverse:self.program(59, program59, data),fn:self.program(26, program26, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                            <div class='info1'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                            <div class='info2'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                        \r\n                            ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content), {hash:{},inverse:self.program(63, program63, data),fn:self.program(61, program61, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                            <input id=\"image-url\" type=\"text\" placeholder=\"Add image URL…\" size=\"15\" />\r\n                            <button id=\"preview\" onclick=\"previewImg()\">Preview</button>\r\n                \r\n                        </div>\r\n\r\n                    ";
  return buffer;
  }
function program59(depth0,data) {
  
  
  return "\r\n                                <input id=\"title-of-pdf\" type=\"text\" placeholder=\"Add a title…\" />\r\n                            ";
  }

function program61(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                                <p class=\"article-abstract\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                            ";
  return buffer;
  }

function program63(depth0,data) {
  
  
  return "\r\n                                <textarea id=\"abstract\" placeholder=\"About the Article…\" rows=\"3\" cols=\"20\"></textarea>\r\n                            ";
  }

function program65(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\r\n\r\n                        <div class=\"article-info\">\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title), {hash:{},inverse:self.program(68, program68, data),fn:self.program(66, program66, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n\r\n                        <div class='info1'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                        <div class='info2'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n\r\n                        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content), {hash:{},inverse:self.program(72, program72, data),fn:self.program(70, program70, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\r\n                        </div>\r\n                    ";
  return buffer;
  }
function program66(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                            <h5 id=\"title\" class=\"article-title\"><a target=\"_blank\" href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a></h5>\r\n                        ";
  return buffer;
  }

function program68(depth0,data) {
  
  
  return "\r\n                            <input id=\"title-of-pdf\" type=\"text\" placeholder=\"Add a title…\" />\r\n                        ";
  }

function program70(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n                            <p class=\"article-abstract\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\r\n                        ";
  return buffer;
  }

function program72(depth0,data) {
  
  
  return "\r\n                            <textarea id=\"abstract\" placeholder=\"About the Article…\" rows=\"3\" cols=\"20\"></textarea>\r\n                        ";
  }

function program74(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\r\n\r\n                    <div class=\"article-info\">\r\n                    <h5 id=\"title\" class=\"article-title\"><a target=\"_blank\" href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.title)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a></h5>\r\n\r\n                    <div class='info1'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publisher)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                    <div class='info2'>"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.publishedDate)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                    \r\n                    <div style='clear: right;'></div>\r\n\r\n                    <div id=\"content\" class=\"article-abstract\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.article),stack1 == null || stack1 === false ? stack1 : stack1.content)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\r\n                    </div>\r\n                ";
  return buffer;
  }

  buffer += "<section class=\"share-template\">\r\n\r\n    <script type=\"text/javascript\" src=\"js/activity.js\"></script>\r\n\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheets/imgviewer.css\">\r\n\r\n    <div class=\"navigation-tabs-container\">\r\n        <ul class=\"tabs-container\">\r\n            <li id=\"tab-facebook\" class=\"tab-left\"><a href=\"javascript:share('facebook')\"><img src=\"http://betapp.newzsocial.com/newzlink/images/Facebook_greycircle_ON_24x24.png\" class=\"hide tab-icon-on\" alt=\"\"/><img src=\"http://betapp.newzsocial.com/newzlink/images/Facebook_greycircle_24x24.png\" class=\"tab-icon-off hide\" alt=\"\"/>Facebook</a></li>\r\n            \r\n            <li id=\"tab-linkedin\" class=\"tab-middle\"><a href=\"javascript:share('linkedin')\"><img src=\"http://betapp.newzsocial.com/newzlink/images/LinkedIn_greycircle_ON_24x24.png\" class=\"hide tab-icon-on\"/><img src=\"http://betapp.newzsocial.com/newzlink/images/LinkedIn_greycircle_24x24.png\" class=\"tab-icon-off\"/>LinkedIn</a></li>\r\n            \r\n            <li id=\"tab-twitter\" class=\"tab-middle\"><a href=\"javascript:share('twitter')\"><img src=\"http://betapp.newzsocial.com/newzlink/images/Twitter_greycircle_ON_24x24.png\" class=\"hide tab-icon-on\" alt=\"\"/><img src=\"http://betapp.newzsocial.com/newzlink/images/Twitter_greycircle_24x24.png\" class=\"tab-icon-off\" alt=\"\"/>Twitter</a></li>\r\n            \r\n            <li id=\"tab-email\" class=\"tab-right\"><a href=\"javascript:share('email')\"><img src=\"http://betapp.newzsocial.com/newzlink/images/Email_greycircle_ON_24x24.png\" class=\"hide tab-icon-on\" alt=\"\"/><img src=\"http://betapp.newzsocial.com/newzlink/images/Email_greycircle_24x24.png\"  class=\"tab-icon-off\" alt=\"\"/>Email</a></li>\r\n        </ul>\r\n    </div>\r\n\r\n    ";
  stack1 = helpers['if'].call(depth0, depth0.signUp, {hash:{},inverse:self.program(11, program11, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\r\n\r\n</section>";
  return buffer;
  });

this["NewzSocial"]["Template"]["signin"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<section class=\"signin-template\">\r\n\r\n    <div class=\"container-header\">\r\n\r\n        <center>\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\">  \r\n        </center>\r\n       \r\n        <!-- <div class=\"header-tab-70\">\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\"> \r\n        </div>\r\n\r\n        <div class=\"header-tab-30\">\r\n            <div class=\"signin-tab \">Sign In</div>\r\n        </div> -->\r\n\r\n    </div>\r\n  \r\n    <div class=\"container-body\">\r\n\r\n        <div class=\"social-btn-container\">\r\n\r\n            <button class=\"social-btn\" onclick=\"NAS.lnSignIn(linkCallBack);return false;\"><span class=\"social-btn-img linkedin-icon\"></span>Sign In With LinkedIn</button>\r\n\r\n            <button class=\"social-btn\" onclick=\"NAS.fbSignIn(fbCallBack);return false;\"><span class=\"social-btn-img facebook-icon\"></span>Sign In With Facebook</button>\r\n\r\n            <button class=\"social-btn\" onclick=\"NAS.twtSignIn(twtCallBack);return false;\"><span class=\"social-btn-img twitter-icon\"></span>Sign In With Twitter</button>\r\n\r\n        </div>\r\n\r\n        <div class=\"signin-input-container\">\r\n          <!-- Email signin field -->  \r\n            <input name=\"userEmail\" id=\"userEmail\" type=\"text\" placeholder=\"Email\" onkeypress=\"enterKey(event, 'signin')\"><br> \r\n            <input name=\"userPwd\" id=\"userPwd\" type=\"password\" placeholder=\"Password\" onkeypress=\"enterKey(event, 'signin')\"><br>\r\n            <button class=\"signin-btn\" onclick=\"signIn()\">Sign In</button>\r\n        \r\n        </div>\r\n\r\n        <hr>\r\n\r\n        <div class=\"switch-page-link\">\r\n            <a href=\"javascript:renderNewzSocialSignUp();\">Create Account</a> | <a href=\"javascript:renderNewzSocialMessage('reset');\">Forgot Password?</a>\r\n        </div>\r\n\r\n    </div>\r\n\r\n</section>";
  });

this["NewzSocial"]["Template"]["signup"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<section class=\"signup-template\">\r\n\r\n    <div class=\"container-header\">\r\n\r\n        <center>\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\">  \r\n        </center>\r\n\r\n        <!-- <div class=\"header-tab-70\">\r\n            <img class='newzsocial-logo' src=\"images/newzsocial.png\" alt=\"newzsocial\">  \r\n        </div>\r\n\r\n        <div class=\"header-tab-30\">\r\n            <div class=\"signup-tab\">Sign Up</div>  \r\n        </div> -->\r\n\r\n    </div>\r\n\r\n    <div class=\"container-body\">\r\n\r\n        <div class=\"signup-input-container\">\r\n        <!-- Email signup field -->  \r\n            <input id=\"userName\" type=\"text\" placeholder=\"Username\" onkeypress=\"enterKey(event, 'signup')\">\r\n            <input id=\"userEmail\" type=\"text\" placeholder=\"Email Address\" onkeypress=\"enterKey(event, 'signup')\">\r\n            <input id=\"userPwd\" type=\"password\" placeholder=\"Password\" onkeypress=\"enterKey(event, 'signup')\">\r\n            <input id=\"userConfirmationPwd\" type=\"password\" placeholder=\"Password Confirmation\" onkeypress=\"enterKey(event, 'signup')\"><br>\r\n            <button class=\"signup-btn\" onclick=\"signUp()\">Sign Up Now!</button>\r\n        </div>\r\n\r\n        <hr>\r\n\r\n        <div class=\"switch-page-link\">\r\n            <a href=\"javascript:renderNewzSocialSignIn();\">Sign In To NewzSocial</a>\r\n        </div>\r\n\r\n    </div>\r\n\r\n<!--     <div class=\"container-footer\">\r\n        <a href=\"#\" class=\"btn\" onclick=\"closeBookmarklet()\">CLOSE</a>\r\n        <a href=\"#\" class=\"btn\" onclick=\"signUp()\">SIGN UP</a>\r\n    </div> -->\r\n\r\n</section>\r\n";
  });