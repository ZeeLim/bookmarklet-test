/********************** PIPE IT FUNCTIONS *************************/

var pipeIt = {
    all         : false,    //LinkedIn share to everyone feature, default value set to "false"
    hasSchedule : false     //by default schedule set to false first
};

function accordionToggle ( type ) {
    // closing the accordion
    if ( $('.accordion-toggle').attr('data-state') == "on" ) {

        $('.accordion-toggle').attr('data-state','off');
        $('.accordion-toggle').removeClass('pipeIt-active');
        $('#pipeButton').hide();
      
        switch ( type ) {
            case "email"    : $('#shareButton-email').show();
            break;
            case "facebook" : $('#shareButton').show();
            break;
            case "linkedin" : $('#shareButton').show();
            break;
            case "twitter"  : $('#shareButton-twt').show();
        }

        setTimeout(function(){autoResize(750);},500);
    }

    // opening the accordion
    else if ( $('.accordion-toggle').attr('data-state') == "off" ) {
        $('.accordion-toggle').attr('data-state','on');
        $('.accordion-toggle').addClass('pipeIt-active');
        $('#shareButton, #shareButton-twt, #shareButton-email').hide();
        $('#pipeButton').show();
        setTimeout(function(){autoResize(750);},500);
    }
    else return;
        console.log("Accordion State :" + $('.accordion-toggle').attr('data-state'));  // to check
}

function checkOaData ( type ) {
    pipeIt = {};
    pipeIt.all = false;
    pipeIt.hasSchedule = false;

    if ( NewzSocial.User.profiles.smc.smcUserId ) {
        var targetType = $('#share-type-select').val();

        switch ( type ) {
            case "facebook" :
                pipeIt.oaType = 2; 
                
                switch ( targetType ) {
                    case "timeline" :
                        pipeIt.oaTargetType  = 21;
                        pipeIt.oaTargetId    = NewzSocial.User.profiles.facebook.detail.oaUserId;
                        pipeIt.oaTargetName  = NewzSocial.User.profiles.facebook.detail.userName+" \'s Timeline";
                    break;
                    
                    case "group"  :
                        pipeIt.oaTargetType  = 22;
                        pipeIt.oaTargetId    = $('#share-group-select').val();
                        pipeIt.oaTargetName  = $("#share-group-select option:selected").text();
                    break;
                    
                    case "page" :
                        pipeIt.oaTargetType  = 23;
                        pipeIt.oaTargetId    = $('#share-page-select').val();
                        pipeIt.oaTargetName  = $("#share-page-select option:selected").text(); 
                    break;
                }
            break;

            case "linkedin" :
                pipeIt.oaType = 4;
      
                switch ( targetType ) {
                    case "connections"  :
                        pipeIt.oaTargetType  = 40;
                        pipeIt.oaTargetId    = NewzSocial.User.profiles.linkedin.detail.oaUserId;
                        pipeIt.oaTargetName  = NewzSocial.User.profiles.linkedin.detail.userName+" \'s Profile";
                    break;

                    case "all"  :
                        pipeIt.oaTargetType  = 41;
                        pipeIt.oaTargetId    = NewzSocial.User.profiles.linkedin.detail.oaUserId;
                        pipeIt.oaTargetName  = NewzSocial.User.profiles.linkedin.detail.userName+" \'s Profile For All";
                        pipeIt.all           = true;   //parameter that allow linkedin to share with everyone!
                    break;
                    
                    case "group"  :
                        pipeIt.oaTargetType  = 42;
                        pipeIt.oaTargetId    = $('#share-group-select').val();
                        pipeIt.oaTargetName  = $("#share-group-select option:selected").text();
                    break;
                    
                    case "page" :
                        pipeIt.oaTargetType  = 43;
                        pipeIt.oaTargetId    = $('#share-page-select').val();
                        pipeIt.oaTargetName  = $("#share-page-select option:selected").text();
                    break;
                }
            break;

            case "twitter"  :
                //twitter so far only has one pipe, no case needed
                pipeIt.oaType        = 3;
                pipeIt.oaTargetType  = 30;
                pipeIt.oaTargetId    = NewzSocial.User.profiles.twitter.detail.oaUserId;
                pipeIt.oaTargetName  = NewzSocial.User.profiles.twitter.detail.userName+" \'s Wall";
            break;
        }
    }
    preCheckSchedule();
}

function preCheckSchedule () {
    var SocialSettings = NewzSocial.User.profiles.smc;
    
    pipeIt.smcUserId = SocialSettings.smcUserId;
    $('#pipeIt-btn-set-schedule').show();
    $('#pipeIt-have-schedule').hide();

    if ( SocialSettings.direct.length ) {

        for ( var i = 0; i < SocialSettings.direct.length; i++ ) {

            if ( SocialSettings.direct[i].oaTargetId == pipeIt.oaTargetId ) {

                pipeIt.smcTouchPointId = SocialSettings.direct[i].tpId;
                pipeIt.pipeId = SocialSettings.direct[i].pipeId;
                console.log("found match for pipe: 1)tpId: ", pipeIt.smcTouchPointId+ " 2)pipeId: " +pipeIt.pipeId); //check

                if ( SocialSettings.direct[i].schedule !== undefined ) {
                    pipeIt.hasSchedule = true;
                    $('#pipeIt-have-schedule').show();  
                    $('#pipeIt-btn-set-schedule').hide();

                    pipeIt.display = {
                        scheduleType : SocialSettings.direct[i].schedule,
                        scheduleP1   : SocialSettings.direct[i].scheduleP1,
                        scheduleP2   : SocialSettings.direct[i].scheduleP2,
                        description  : SocialSettings.direct[i].desc
                    }
                }
            break;               
            }
        }
    }
}

function pipeArticle () {
    var comments;
    
    if ( $('#share-user-comment').val() ) comments = $('#share-user-comment').val();
    else comments = "";

    var params = {
            articleId       : NewzSocial.Article.id,
            headline        : NewzSocial.Article.title,
            comment         : comments,
            link            : NewzSocial.Article.url,
            image           : NewzSocial.Article.imageUrl,
            pipelineId      : pipeIt.pipeId,
            smcTouchPointId : pipeIt.smcTouchPointId,
            smcUserId       : pipeIt.smcUserId,
            all             : pipeIt.all,
          };

    if ( $("input[value='pipe-schedule']:checked").val() ) {
        
        // if user indicated to use pipe schedule but have none set up
        if ( !pipeIt.hasSchedule ) displayMessage("Error!", "Please set a new schedule.", 1, 1500);
        else {  // pipe with existing schedule
            console.log("Schedule query",params);
            NewzSocial.API_POST( bookmarklet.appHost, 'nscon', 'd', 'DirectPostBySchedule', params, 6, callback );
        }
    } //end of schedule pipe
        
    // else if user defined delayed post
    else if ( $("input[value='user-defined']:checked").val() ) {
      
        params.minutesDelay = $('#delay-define').val()*$('#select-delay').val();
        console.log("Delay query",params);

        // if user does not have respective schedule for direct post, create direct post touchpoint
        // as user may or may not have respective touchpoint enabled
        if ( !pipeIt.hasSchedule ) {
            console.log("pipeIt.hasSchedule false");
            var secondParams = {
                    oaType          : pipeIt.oaType,
                    oaTargetType    : pipeIt.oaTargetType,
                    oaTargetId      : pipeIt.oaTargetId,
                    oaTargetName    : pipeIt.oaTargetName,
                    smcUserId       : pipeIt.smcUserId
                };
            // create touchpoint -> then update direct post touchpoint id after touchpoint is created -> then execute direct post
            postAfter_EnableDirectPostsTouchPoint( params, secondParams );
        }
        else {      // else if user have schedule already set
            params.pipelineId = pipeIt.pipeId;
            params.smcTouchPointId = pipeIt.smcTouchPointId;
        
            renderNewzSocialMessage("sharing");
            NewzSocial.API_POST( bookmarklet.appHost, 'nscon', 'd', 'DirectPostAfter', params, 6, callback2 );
        }
    }

    else {
        console.log("nothing is check!");
        displayMessage("Error!", "Please select an option", 1, 1500);
    }

    function callback ( callNumber, response ) {
        renderNewzSocialMessage("sharing");

        if ( response.status ) {
            console.log("DirectPostBySchedule successful");
            renderNewzSocialMessage("transition");
            displayMessage("Successful!","Article Piped!",2,1500);
            setTimeout(function(){window.close();},1500);
        }
        else {
            console.log( "Error with DirectPostBySchedule API: ", response );
            displayMessage("Error!", "Please try again later!", 1, 1500);
        }
    }

    function callback2 ( callNumber, response ){
        if ( response.status ) {
            console.log("DirectPostAfter successful");
            renderNewzSocialMessage("transition");
            displayMessage("Successful!","Article Piped!",2,1500);
            setTimeout(function(){window.close();},1500);
        }
        else {
            console.log( "Error with DirectPostAfter(with schedule) API: ", response );
            displayMessage("Error!", "Please try again later!", 1, 1500);
        }
    }
}
      
function postAfter_EnableDirectPostsTouchPoint ( info1, info2 ) {
    var pipeId, 
        touchPointId,
        params = info1;

    //if either pipeid or tpid is undefined, meaning GetSocialSettings no pipe info
    if ( !params.pipelineId || !params.smcTouchPointId )  
        NewzSocial.API_POST( bookmarklet.appHost,'nscon','d','EnableDirectPostsTouchPoint', info2, 6, callback1);
    else 
        NewzSocial.API_POST( bookmarklet.appHost,'nscon','d','DirectPostAfter', info1, 6, callback3);

    function callback1 ( callNumber, response ) {
        if ( response.status ) { 
            touchPointId = response.oaTPId;
            NewzSocial.API_POST( bookmarklet.appHost, 'nscon', 'd', 'GetSocialSettings', {}, 6, callback2);
        }
        else {
            console.log( "Error with EnableDirectPostsTouchPoint API: ", response );
            displayMessage( "Error!", "Please try again later!", 1, 1500 );
        }
    }

    function callback2 ( callNumber, response ) {
        if ( response.status ) {
            for ( var i = 0; i < response.data.direct.length; i++ ) {
                if ( response.data.direct[i].tpId == touchPointId ) pipeId = response.data.direct[i].pipeId;
                break;
            }

            if ( touchPointId && pipeId ) {
                params.pipelineId = pipeId;
                params.smcTouchPointId = touchPointId;

                NewzSocial.API_POST( bookmarklet.appHost,'nscon','d','DirectPostAfter', params, 6, callback3);
            }
        }
        else {
            console.log( "Error with GetSocialSettings API: ", response );
            displayMessage( "Error!", "Please try again later!", 1, 1500 );
        }
    }

    function callback3 ( callNumber, response ) {
        renderNewzSocialMessage("sharing");

        if ( response.status ) {
            console.log("DirectPostAfter successful");
            renderNewzSocialMessage("transition");
            displayMessage("Successful!","Article Piped!",2,1500);
            setTimeout(function(){window.close();},1500);
        }
        else {
            console.log ( "Error with DirectPostAfter(without schedule) API: ", response );
            displayMessage( "Error!", "Please try again later!", 1, 1500 );
        }
    }
}

/******************************* SCHEDULE RELATED FUNCTIONS STARTS HERE *******************************/

function toggleScheduleTypeBtn () {

    //when user click choose schedule type
    $('.set-nav-btn').click( function () {
        $('.set-nav-btn').removeClass('set-nav-btn-active');
        $(this).addClass('set-nav-btn-active');
        $('.container-specify-schedule').hide();        // hide all schedule specification containers

        var correspondingClass = $(this).attr('id');      
        $('#' + correspondingClass + '-container').show();           // show the specification container chosen
        
        pipeIt.schdType = parseInt($('.set-nav-btn-active').attr('data-schdType'));  // updates pipeIt.schdType
    } );

}

function appendTimeSlot ( hour, minute, day ) {

    var h = "", m = "", d = "";

    if ( hour != undefined ) h = hour;
    if ( minute != undefined ) m = minute;
    if ( day != undefined ) d = day;

    var timeSlotHtml="";
        timeSlotHtml+="<div class='element-timeslot'>";
        timeSlotHtml+="<div class='input-prepend'>";
        timeSlotHtml+="<input class='timeslot-hours' type='text' onkeypress='return isNumberKey(event)' placeholder='00' value='" + h + "'>";
        timeSlotHtml+="<span class='middle-colon'>:</span>";
        timeSlotHtml+="<input class='timeslot-minutes' type='text' onkeypress='return isNumberKey(event)' placeholder='00' value='" + m + "'>";
        timeSlotHtml+="<select>";
        timeSlotHtml+="<option value=''>Everyday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 1)?"selected ":""; timeSlotHtml+= "value='mon-'>Every Monday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 2)?"selected ":""; timeSlotHtml+= "value='tue-'>Every Tuesday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 3)?"selected ":""; timeSlotHtml+= "value='wed-'>Every Wednesday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 4)?"selected ":""; timeSlotHtml+= "value='thu-'>Every Thursday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 5)?"selected ":""; timeSlotHtml+= "value='fri-'>Every Friday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 6)?"selected ":""; timeSlotHtml+= "value='sat-'>Every Saturday</option>";
        timeSlotHtml+="<option "; timeSlotHtml+= (d == 7)?"selected ":""; timeSlotHtml+= "value='sun-'>Every Sunday</option>";
        timeSlotHtml+="</select>";
        timeSlotHtml+= "<span class='timeslot-remove' onclick='removeParentElement(event);'>&times</span>";
        timeSlotHtml+="</div>";
        timeSlotHtml+="</div>";

    $('#container-specific-timeslots').append(timeSlotHtml);

    $( ".timeslot-hours, .timeslot-minutes" ).blur( function () {
        if ( $(".timeslot-hours").val() > 23 ) $(".timeslot-hours").val("23");
        if ( $(".timeslot-minutes").val() > 59 ) $(".timeslot-minutes").val("59");
    });
}   

function setSchedule () {
        
    var scheduleP1 = null, scheduleP2 = null;

    switch ( pipeIt.schdType ) {
        // frequency
        case 1: 
            scheduleP1 = $('#frequency-define').val();            // value
            scheduleP2 = $('#frequency-denom').find(":selected").text();  // minutes||hours||days
        break;
      
        // random
        case 2:
            scheduleP1 = $('#random-define-1').val(); // number of timeslots to generate
            scheduleP2 = $('#random-define-2').val()*$('#random-denom').val()/$('#random-denom').find('option:selected').attr('data-divisor');  // time in hours
        break;
      
        // specified
        case 3:
            scheduleP1 = 0;
            scheduleP2 = '';
            $('.element-timeslot').each( function(i){
            
                // for each element with non-empty fields
                if ( $(this).find('.timeslot-hours').val() && $(this).find('.timeslot-minutes').val() ) {
                    scheduleP1++;
                    scheduleP2 += $(this).find('option:selected').val() + $(this).find('.timeslot-hours').val() + ":" + $(this).find('.timeslot-minutes').val() + ",";
                }
                else {
                    displayMessage("Error!", "Please complete the schedule!", 1, 1500);
                }
            });
            scheduleP2 = scheduleP2.slice(0,-1);  // remove last comma
        break;
    }

    var params = {
        smcUserId    : pipeIt.smcUserId,
        oaType       : pipeIt.oaType,
        oaTargetType : pipeIt.oaTargetType,
        oaTargetId   : pipeIt.oaTargetId,
        oaTargetName : pipeIt.oaTargetName,
        schdType     : pipeIt.schdType,
        scheduleP1   : parseInt(scheduleP1),
        scheduleP2   : scheduleP2
    };
    console.log(params); // check

    NewzSocial.API_POST(bookmarklet.appHost,'nscon','d','EnableDirectPostsTouchPoint', params, 6, callback);

    function callback ( callNumber, response ) {
        if ( response.status ) {
            console.log( "EnableDirectPostsTouchPoint successful!" );
            updateSocialSettings ( NewzSocial.User.uToken );
            displayMessage( "Successful!","Schedule set!", 2, 1500 );
        }
        else {
            console.log ( response );
            displayMessage( "Error!", "Please try again later!", 1, 1500 );
        }
        $('#pipe-schedule-modal').modal('hide');
    }    
}

function showPipeScheduleModal () {
    $('.schedule-post-container > span > input').attr("checked","checked");
    $('#pipe-schedule-modal').modal('show');
    $('#title-schedule').text('Set Schedule For ' + pipeIt.oaTargetName );

    $('.container-specify-schedule').hide();
    toggleScheduleTypeBtn();

    if ( pipeIt.hasSchedule ) {
        pipeIt.schdType = pipeIt.display.scheduleType;  //Existing schedule type
        showExistingSchedule();

        //Front End Manipulation
        $('.container-show-schedule').show();
        
    }
    else {
        pipeIt.schdType = 1;  
     
        //Front End Manipulation
        $('.container-show-schedule').hide();
        $('.set-nav-btn').removeClass('set-nav-btn-active');
        $('#set-frequency').addClass('set-nav-btn-active');
        $('#set-frequency-container').show(); //Default/New schedule type 
        $('#frequency-define').val("4");

        $('#container-specific-timeslots').html('');
        appendTimeSlot();
    }
}

function showExistingSchedule () {
    
    var oaTarget = "", schedule = "", specificTimeArray = [], 
        scheduleType = pipeIt.display.scheduleType,
        P1 = pipeIt.display.scheduleP1, 
        P2 = pipeIt.display.scheduleP2;

    $('#container-specific-timeslots').html('');
    $('.set-nav-btn').removeClass('set-nav-btn-active');

    switch ( scheduleType ) {     //pre-process frequency schedule type
        case 1 :

            $('#set-frequency').addClass('set-nav-btn-active'); 
            $('#set-frequency-container').show();

            $('#frequency-define').val( P1 );

            switch ( P2 ) {
                case "hours" : $('#frequency-denom').val("60");
                break;
                case "minutes" : $('#frequency-denom').val("1");
                break; 
                case "days" : $('#frequency-denom').val("1440");
            }

            appendTimeSlot();

            schedule += "Post an article every " + P1; 
            schedule += " " + P2;
        
        break;
    
        case 2 :    //pre-process random schedule type

            $('#set-random').addClass('set-nav-btn-active');
            $('#set-random-container').show();

            $('#random-define-1').val( P1 ); 
            $('#random-define-2').val( P2 );   //system won't allow integer less than 1 
            //random-denome by default show "hours"

            appendTimeSlot();

            schedule += "Post " + P1;
            schedule += " article(s) in every " + P2 + " hours";
        
        break;
    
        case 3 :      //pre-process specific schedule type

            $('#set-specific').addClass('set-nav-btn-active');
            $('#set-specific-container').show();

            for ( var i = 0; i < P1; i++ ) {

                //For display purpose
                var tmpArray = P2[0][i].split("-"), postArray = [],
                    t, h, m, d;    //For Schedule resetting 

                switch ( tmpArray.length ) {
                    case 1 :
                        postArray = ["Everyday", tmpArray[0]];
                        t = tmpArray[0].split(":");
                        d = 0;
                    break;

                    case 2 :
                        switch ( tmpArray[0] ) {
                            case "mon" : postArray = ["Every Monday", tmpArray[1]]; d = 1;
                            break;
                            case "tue" : postArray = ["Every Tuesday", tmpArray[1]]; d = 2;
                            break;   
                            case "wed" : postArray = ["Every Wednesday", tmpArray[1]]; d = 3;
                            break;
                            case "thu" : postArray = ["Every Thursday", tmpArray[1]]; d = 4;
                            break;
                            case "fri" : postArray = ["Every Friday", tmpArray[1]]; d = 5;
                            break;
                            case "sat" : postArray = ["Every Saturday", tmpArray[1]]; d = 6;
                            break;
                            case "sun" : postArray = ["Every Sunday", tmpArray[1]]; d = 7;
                        }

                        t = tmpArray[1].split(":");

                    break;        
                }

                switch ( t.length ) {
                    case 1 : h = t[0]; m = 00;
                    break;
                    case 2 : h = t[0]; m = t[1];
                    break;
                }

                appendTimeSlot( h, m, d );
                specificTimeArray.push(postArray);
            }

            schedule += "Post an article on <br>";
            for ( var i = 0; i < specificTimeArray.length; i++ ) {
                schedule += specificTimeArray[i][0] + " at " + specificTimeArray[i][1] + "<br>";
            }

        break;
    }

    switch ( pipeIt.oaType ) {
        case 2 : $('#oa-image').attr("src", "images/Facebook_greycircle_24x24.png");
        break;
        case 3 : $('#oa-image').attr("src", "images/Twitter_greycircle_24x24.png");
        break;
        case 4 : $('#oa-image').attr("src", "images/LinkedIn_greycircle_24x24.png");
    }

    if ( pipeIt.oaTargetType == 22 || pipeIt.oaTargetType == 42 ) oaTarget = "Group: ";
    if ( pipeIt.oaTargetType == 23 || pipeIt.oaTargetType == 43 ) oaTarget = "Page: ";
    oaTarget += pipeIt.oaTargetName;

    $('#oa-schedule > span').html("<b>" + oaTarget +": </b>" + schedule);

}

function updateSocialSettings ( uToken ) {
    NewzSocial.API('GetSocialSettings', 'd', null, uToken, [{'argument':'dummy', 'parameter':'dummy'}], 6, callback);

    function callback ( callNumber, response ) {
        if ( response.status ) {
            var SocialSettings = response.data;

            if ( SocialSettings.direct.length ) {

                for ( var i = 0; i < SocialSettings.direct.length; i++ ) {

                    if ( SocialSettings.direct[i].oaTargetId == pipeIt.oaTargetId ) {

                        pipeIt.smcTouchPointId = SocialSettings.direct[i].tpId;
                        pipeIt.pipeId = SocialSettings.direct[i].pipeId;
                        console.log("found match for pipe: 1)tpId: ", pipeIt.smcTouchPointId+ " 2)pipeId: " +pipeIt.pipeId); //check

                        if ( SocialSettings.direct[i].schedule !== undefined ) {
                            pipeIt.hasSchedule = true;
                            $('#pipeIt-have-schedule').show();  
                            $('#pipeIt-btn-set-schedule').hide();

                            pipeIt.display = {
                                scheduleType : SocialSettings.direct[i].schedule,
                                scheduleP1   : SocialSettings.direct[i].scheduleP1,
                                scheduleP2   : SocialSettings.direct[i].scheduleP2,
                                description  : SocialSettings.direct[i].desc
                            }
                        }
                    break;
                    }
                }
            }
        }
    }
}

function isNumberKey ( event ) {

   var charCode = (event.which) ? event.which : event.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
   return true;
}

function autoCheckDirectPostOption () {
    $('.direct-post-container > span > input').attr("checked","checked");
}

function removeParentElement(event){
    $(event.target.parentNode).remove();
}
