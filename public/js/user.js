/********************** CONSTRUCT USER API *********************/

NewzSocial.BuildUser = function(uToken){
  	NewzSocial.User = {
	    uToken   : uToken,
	    profiles : {},
	    display  : {}
  	};

  	NewzSocial.API('GetProfiles', 'u', null, uToken, null, 0, collector);
 	NewzSocial.API('GetNewzstandUserGraph', 'u', null, uToken, null, 1, collector);
  	NewzSocial.API('GetGroupsForUser', 'u', null, uToken,[{'argument': 'oaType', 'parameter': '2'}],2,collector);
  	NewzSocial.API('GetGroupsForUser', 'u', null, uToken,[{'argument': 'oaType', 'parameter': '4'}],3,collector);
  	NewzSocial.API('GetPages', 's', null, uToken,[{'argument': 'oaType', 'parameter': '2'}],4,collector);
  	NewzSocial.API('GetPages', 's', null, uToken,[{'argument': 'oaType', 'parameter': '4'}],5,collector);
  	NewzSocial.API('GetSocialSettings','d', null, uToken, null, 6, collector);

  	var counter          = 7,
      	profileResponses = [],
      	tmpProfile       = NewzSocial.User.profiles;
         
  	function collector ( callNumber, response ) {
	    counter--;
	    profileResponses[callNumber]= response;

    	if ( counter == 0 ) {
      		var GetProfiles = profileResponses[0].data;

      		for ( var i = 0; i < GetProfiles.length; i++) {
        		
        		switch(GetProfiles[i].type){
			        case 1:
			          	tmpProfile.email = {
			            	friends           : [],
			            	groups            : [],
			            	sendListFriend    : [],
			            	sendListNonFriend : [],
			            	detail            : GetProfiles[i]
			          	}
			          	break;

		          	case 2:
		          		tmpProfile.facebook = {
		            		friends : [],
		            		groups  : [],
		            		pages   : [],
		            		detail  : GetProfiles[i]
		          		}
		          		break;
          
          			case 3:
          				tmpProfile.twitter = {
            				friends : [],
            				groups  : [],
            				detail  : GetProfiles[i]
          				}
          				break;

          			case 4:
          				tmpProfile.linkedin = {
            				friends : [],
            				groups  : [],
            				pages   : [],
            				detail  : GetProfiles[i]
          				}
          				break;
        		}
      		} // end of for loop

		    //For display purposes only (put into one object to be passed into template)
		    var tmpDetail = tmpProfile.linkedin || tmpProfile.facebook || tmpProfile.twitter;
		    NewzSocial.User.display = {
		        userName : (tmpDetail!==undefined && tmpDetail.detail.userName!==null)?tmpDetail.detail.userName:tmpProfile.email.detail.userName,
		        img      : (tmpDetail!==undefined && tmpDetail.detail.imageUrl!==null && tmpDetail.detail.imageUrl!=="")?tmpDetail.detail.imageUrl:'images/social-icons/email-profile-image.jpg',
		        email    : tmpProfile.email==undefined?"":tmpProfile.email.detail.email //only using email sign_up 
		      };

        
      		var NewzStandUserGraph = profileResponses[1].data;
      		for ( var i = 0; i < NewzStandUserGraph.length; i++ ) {
        		var detector = [],
        		user = NewzStandUserGraph[i];

       			for ( var j = 0; j < user.oaDetails.length; j++ ) {
          			var oaProfile = user.oaDetails[j];

          			switch(oaProfile.oaType){
            			case 1:
            				if(tmpProfile.email!==undefined){
				              	tmpProfile.email.friends.push({"userId":user.userId,"oaProfile":oaProfile});
				              	detector['defaultUserName'] = oaProfile.userName;
				              	detector['email'] = oaProfile.email;
            				}
            				break;

            			case 2:
            				if(tmpProfile.facebook!==undefined){
             					 detector['facebook'] = oaProfile.userName;
              					if(_.contains(user.friendshipTypes, 2)){
                					tmpProfile.facebook.friends.push({"userId":user.userId,"oaProfile":oaProfile});
              					}
            				}
            				break;

            			case 3:
            				if (tmpProfile.twitter!==undefined) {
              					detector['twitter'] = oaProfile.userName;
              					if(_.contains(user.friendshipTypes, 3)){
                					tmpProfile.twitter.friends.push({"userId":user.userId,"oaProfile":oaProfile});
              					}
            				}
            				break;

            			case 4:
            				if (tmpProfile.linkedin!==undefined) {
              					detector['linkedin'] = oaProfile.userName;
              					if(_.contains(user.friendshipTypes, 4)){
                					tmpProfile.linkedin.friends.push({"userId":user.userId,"oaProfile":oaProfile});
              					}
            				}
            				break;
          			}
        		} // user for loop ends here
        		
        		getEmailFriendSendList(detector, user);
      
      		} //newzstandusergraph for loop ends here

      		//Get history email for Non-NewzSocial account, used by email select option
      		getEmailNonFriendSendList(NewzSocial.User.uToken);

		    if ( profileResponses[6].data.length!=0 ) tmpProfile.smc = profileResponses[6].data;
		    if ( profileResponses[2].data.length!=0 ) tmpProfile.facebook.groups = profileResponses[2].data;
		    if ( profileResponses[3].data.length!=0 ) tmpProfile.linkedin.groups = profileResponses[3].data;
		    if ( profileResponses[4].status ) tmpProfile.facebook.pages = profileResponses[4].data;
		    if ( profileResponses[5].status ) tmpProfile.linkedin.pages = profileResponses[5].data;
    	}
  	}  
};

function getEmailFriendSendList(detector, user){
	if(detector["email"] != undefined){
		var params = {
			userId  : user.userId,
			email   : detector['email']
		}
		if(detector["linkedin"] != undefined){
			params.displayUserName = detector['linkedin'];
			NewzSocial.User.profiles.email.sendListFriend.push(params);
		}else if(detector["facebook"] != undefined){
			params.displayUserName = detector['facebook'];
			NewzSocial.User.profiles.email.sendListFriend.push(params);    
		}else if(detector["twitter"] != undefined){
			params.displayUserName = detector['twitter'];
			NewzSocial.User.profiles.email.sendListFriend.push(params);
		}else{
			params.displayUserName = detector['defaultUserName'];
			NewzSocial.User.profiles.email.sendListFriend.push(params);
		}
	}
}

function getEmailNonFriendSendList (uToken){

	NewzSocial.API('GetUserProperty', 'user', null, uToken, [{'argument': 'property', 'parameter':'savedEmails'}],0,function(callNumber,response){
		if(response.data !== undefined){
			email = response.data.split(',');
			for ( var i = 0; i < email.length; i++){

				NewzSocial.User.profiles.email.sendListNonFriend.push({id:email[i],text:email[i]});
			}
		}
	});
}


/********************* USERS SIGN IN/SIGN UP FUNCTIONS ********************/

//GENERAL SIGN UP FUNCTIONS
function signUp ( merging ) {

  	var new_uToken,
      	userEmail =  $("#userEmail").val();

  	if ( $('#userName').val() == "" ) 
      	displayMessage("Error!", "Please Enter Your Username!",1 ,1500);

  	else if ( $('#userPwd').val() !== $('#userConfirmationPwd').val()) 
      	displayMessage("Error!", "Please Re-type Your Password!",1 ,1500);

  	else 
      	NewzSocial.API('SignUp', 'user', null, null, [{'argument': 'login', 'parameter': $("#userEmail").val()}, {'argument':'password', 'parameter': $("#userPwd").val()}],0,callback1);

	function callback1 ( callNumber, response ) {
	    if (response.status) {
	         new_uToken = response.data.uToken;
	         NewzSocial.API('UpdateProfile', 'user', null, new_uToken, [{'argument': 'name', 'parameter': $('#userName').val()}, {'argument':'oaType', 'parameter':1}], 0, callback2);
	    } 
	    else {
	        if ( response.code == 39 || response.code == 3 ) displayMessage("Error!","Invalid Email/Password",1,1500);
	        else if ( response.code == 11 ) displayMessage("Error!","Email Has Already Been Registered!",1,1500);
	        console.log(response.message);
	    } 
	}

	function callback2 ( callNumber, response ) {
	    if(response.status) console.log('username update successful!');
	    else console.log ("username update failed!");

	    if ( merging ) {
	        signUpEmailAndMerging( new_uToken );
	    }
	    else {
	        renderNewzSocialMessage("transition");
	        displayMessage("Successful!","NewzSocial Account Created!",2,1500);
	        setTimeout(function(){
	            renderNewzSocialNonVerified(true, userEmail);
	        },1500);
	    }
	}
}


//GENERAL SIGN IN FUNCTIONS
function signIn () {

  	var userEmail = $("#userEmail").val(),
      	uToken;

  	NewzSocial.API('SignIn', 'user', null, null,[{'argument': 'login', 'parameter': $("#userEmail").val()}, {'argument': 'password', 'parameter': $("#userPwd").val()}],0,callback1);

  	function callback1 ( callNumber, response ) {
      	if ( response.status ) {

          	uToken = response.data.uToken;
          	renderNewzSocialMessage("transition");
          	displayMessage("Successful!","Logged In!",2,1500);

          	setTimeout(function(){
              	assignCookie( uToken );
              	if ( document.domain == "localhost" ) {
                  	$.cookie('newzsocial_uToken', uToken, { expires: 365 });
              	}
              	else {
                  	$.cookie('newzsocial_uToken', uToken, { expires: 365, path: '/', domain: 'newzsocial.com' });
              	}

              	console.log('successful email signin'); 
              	NewzSocial.BuildUser(uToken);
              	NewzSocial.GetArticle(parsedUrls, uToken);
          	},1500);
     	} 
      	else if ( response.message == "Email address not verified" ) {
          	renderNewzSocialNonVerified(false, userEmail);
          	displayMessage("Error!", "Email address has not been verified.", 1, 1500);
      	}	
      	else {
          	displayMessage("Error!", "Invalid Email/Password", 1, 1500);
          	console.log(response.message);
      	}	 
  	}
}

function signout () {

	if ( document.domain == "localhost" ) {
		$.cookie('nz_u', null, { expires: -1 });
		$.cookie('nz_a', null, { expires: -1 });
		$.cookie('nz_sr', null, { expires: -1 });
		$.cookie('newzsocial_uToken', null, { expires: -1 });
	}
	else {
		$.cookie('nz_u', null, { expires: -1, path: '/', domain: 'newzsocial.com' });
		$.cookie('nz_a', null, { expires: -1, path: '/', domain: 'newzsocial.com' });
		$.cookie('nz_sr', null, { expires: -1, path: '/', domain: 'newzsocial.com' });
		$.cookie('newzsocial_uToken', null, { expires: -1, path: '/', domain: 'newzsocial.com' });
	}
          // document.cookie = 'newzsocial_uToken' + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
          
        renderNewzSocialMessage("transition");
        displayMessage("Successful!","Logged Out!",2,1500);
        setTimeout(function(){window.close();},1500);
}

function closeBookmarklet () {
   	window.close();
}

//SOCIAL MEDIA SIGN IN STARTS HERE
function linkCallBack ( resp ) {
	console.log("LinkedIn call back: ", resp);
	signInTransition(resp);
}

function fbCallBack ( resp ) {
	console.log("FB call back: ", resp);
	signInTransition(resp);
}

function twtCallBack ( resp ) {
	console.log('Twitter call back');
	signInTransition(resp);
}

function signInTransition ( resp ) {
	if ( resp && resp.status ) {
		renderNewzSocialMessage("transition");
		displayMessage("Successful!","Logged In!",2,1500);
          
        var nz_u = $.cookie('nz_u'),
          	nz_a = $.cookie('nz_a');

        $.cookie('newzsocial_uToken', nz_u, { expires: 365 });
          
		setTimeout(function(){
			NewzSocial.BuildUser(nz_u);
			NewzSocial.GetArticle(parsedUrls,nz_u);
		},1500);
	}
	else {
		displayMessage("Error", resp,1,1500);
	}
}

// function linkUpdateCallBack(resp) {
// 	console.log('In update call back');
// 	console.log(resp);
// 	if (resp && resp.status) {
// 		displaySuccess("Updated Profile!","Loading...",500000);
// 		document.location.href = document.location.href;
// 	} else {
// 		showError(resp.data);
// 	}
// }

// function fbUpdateCallBack(resp) {
// 	$('#loading-modal').modal('hide');
// 	console.log('FB Update call back');
// 	console.log(resp);
// 	if (resp && resp.status) {
// 		displaySuccess("Updated Profile!","Loading...",500000);
// 		document.location.href = document.location.href;
// 	} else {
// 		showError(resp.mycustmessage);
// 	}
// }

//USER LOGIN RELATED FUNCTIONS
function signInAsGuest () {
	var aToken = NewzSocial.Application.aToken;

	var callback = function ( callNumber,response ) {
		if (response.status){
			$.cookie('guest_uToken', response.data.uToken);
			assignCookie( response.data.uToken );
		}
		renderNewzSocialSignIn();     
	};

	NewzSocial.API('SignInAsGuest', 'user', null, null, null, 0, callback);
}

function checkForGuestUser ( uToken ) {

	NewzSocial.API('IsGuest', 'user', null, uToken, null, 0, callback1);

	function callback1 ( callNumber, response ) {

		if ( response.status && !response.data ) {
			NewzSocial.API('IsVerified', 'user', null, uToken, null, 0, callback2); 
		} else {
			signInAsGuest();
		}
	};

	function callback2 ( callNumber, response ) {

		if ( response.status && response.data ) {
			var cookieExpiryDate=new Date();
			cookieExpiryDate.setDate(cookieExpiryDate.getDate() + 365);
			document.cookie='newzsocial_uToken='+uToken+'; expires='+cookieExpiryDate.toUTCString();

			NewzSocial.BuildUser(uToken);
			NewzSocial.GetArticle(parsedUrls, uToken)
		} else {
			signInAsGuest();
		}
	};
}

function resendEmailVerification ( email ) { 
	NewzSocial.API('ResendVerificationEmail', 'user', null, null, [{'argument': 'login', 'parameter': email }], 0,callback);

	function callback ( callNumber, response ) {
		if ( response.status ) displayMessage("Successful!", "An Email Has Been Sent To " + email, 2, 2500);
	}
}

function resetPassword () {
	var email = $('#reset-userEmail').val();

	if ( email != null ) NewzSocial.API('ResetPassword', 'user', null, null, [{'argument': 'login', 'parameter': email }], 0,callback);
	else displayMessage('Error!', "Please fill in your email!", 1, 1500);

	function callback ( callNumber, response ) {
		if ( response.status ) displayMessage("Successful!", "New Password Has Been Sent To " + email, 2,2500);
		else {
			displayMessage("Error!", "Please try again later!", 1, 1500);
			console.log("Error With ResetPassword API: ", response);
		}
	} 
}


function switchMergingPage ( merge ) {
	if ( merge ) {
		$('.account-merging-container').show()
		$('.signup-email, .account-merging-non-verified-container').hide();
	}
	else {
		$('.signup-email').show();
		$('.account-merging-container, .account-merging-non-verified-container').hide();  
	}
}

function signUpEmailAndMerging ( uToken ) {

	var oaTypeKey,
		profilesCount,
		tmp_uToken = uToken,
		socialMediaProfiles = NewzSocial.User.profiles;

	oaTypeKey = _.keys( socialMediaProfiles );
	profilesCount = oaTypeKey.length;
	console.log( "user's profiles to be merged: ", oaTypeKey );

	displayMessage("Successful!","Please wait...", 3,15000000);
	for ( var i = 0; i < oaTypeKey.length; i++ ) {
		profilesCount--;

		if ( oaTypeKey[i] != "smc" ) {
			var params = {
				oaType    : NewzSocial.User.profiles[oaTypeKey[i]].detail.type,
				oaUserId  : NewzSocial.User.profiles[oaTypeKey[i]].detail.oaUserId,
				merge     : true,
				uToken    : tmp_uToken,
				aToken    : NewzSocial.Application.aToken
			};

			$.ajax({
				type: "POST",
				url: "http://" + bookmarklet.engineHost + '/newz/u/SignInUsingOAuth',
				data: params,
				success: function ( response ) {
					if ( response.status ) {
						tmp_uToken = response.data.uToken;
						console.log("merging successful! ", oaTypeKey[i], " profile");

						if ( profilesCount == 0 ) {
							displayMessage("Successful!", "Your Accounts have been merged together!", 2, 1500);
							assignCookie(tmp_uToken);
	                      	NewzSocial.User.display = {}; // clear up the old display
	                      	NewzSocial.BuildUser(tmp_uToken);
                      
		                    setTimeout(function () {
		                      	renderNewzSocialAction();
		                    }, 1500);
                  		}
              		}
		            else {
		              	console.log( "Error with SignInUsingOAuth API: ", response );
		              	displayMessage("Error!", "Please try again later!", 1, 1500); 
		            }
          		}
      		});
        }   //using SignInUsingOAuth API 
    }   //for loop
}

function mergeSocialMediaProfiles () {
	var tmp_uToken,
		tmp_userEmail = $("#userEmail-signin").val();

	NewzSocial.API('SignIn', 'user', null, null,[{'argument': 'login', 'parameter': $("#userEmail-signin").val()}, {'argument': 'password', 'parameter': $("#userPwd-signin").val()}],0,callback1);

	function callback1 ( callNumber, response ) {
		if ( response.status ) {

			var oaTypeKey,
				profilesCount,
				tmp_uToken = response.data.uToken,
				socialMediaProfiles = NewzSocial.User.profiles;

			oaTypeKey = _.keys( socialMediaProfiles );
			profilesCount = oaTypeKey.length;
			console.log( "user's profiles to be merged: ", oaTypeKey );

			for ( var i = 0; i < oaTypeKey.length; i++ ) {
				profilesCount--;

				if ( oaTypeKey[i] != "smc" ) {
					var params = {
						oaType    : NewzSocial.User.profiles[oaTypeKey[i]].detail.type,
						oaUserId  : NewzSocial.User.profiles[oaTypeKey[i]].detail.oaUserId,
						merge     : true,
						uToken    : tmp_uToken,
						aToken    : NewzSocial.Application.aToken
					};

					$.ajax({
						type: "POST",
						url: "http://" + bookmarklet.engineHost + '/newz/u/SignInUsingOAuth',
						data: params,
						success: function ( response ) {
							if ( response.status ) {
								tmp_uToken = response.data.uToken;
								console.log("merging successful! ", oaTypeKey[i], " profile");

								if ( profilesCount == 0 ) {
									displayMessage("Successful!", "Your Account has been merged together!", 2, 1500);
									assignCookie(tmp_uToken);
			                        NewzSocial.User.display = {}; // clear up the old display
			                        NewzSocial.BuildUser(tmp_uToken);

			                        setTimeout(function () {
			                          	renderNewzSocialAction();
			                        }, 1500);
                      			}
                  			}
			                else {
			                  	console.log( "Error with SignInUsingOAuth API: ", response );
			                  	displayMessage("Error!", "Please try again later!", 1, 1500); 
			                }
              			}
          			});
                }   //using SignInUsingOAuth API 
            }   //for loop
        } 
      	else if ( response.message == "Email address not verified" ) {
      		displayMessage("Error!", "Email address has not been verified.", 1, 1500);
      		$('.account-merging-non-verified-container').show();
      		$('.signup-email, .account-merging-container').hide();
      		$('#resendEmail').attr('onclick','resendEmailVerification(' + tmp_userEmail + ')')
      	}
        else {
          	displayMessage("Error!","Invalid Email/Password",1,1500);
          	console.log(response.message);
        }
   	}
}
