/******************** START OF NEWZSOCIAL ACTIONS FUNCTIONS **********************/

//GENERAL FUNCTIONS FOR NEWZSOCIAL ACTIONS
function HTMLDecode ( s ) {
    return s.replace(/&rsquo;/g, "'").replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&hellip;/g,'...');
}

// global variable to determine PDF
var PDFimg = new String();

function displayArticleInformation () {

    var tmpContent,
        tmpPublisher,
        publishedDate;

    var isPDF = false,
        isImg = false,
        i = 0;
    var tmp = [];    

    tmpContent = NewzSocial.Article.content || NewzSocial.Article.description || NewzSocial.Article.articleAbstract || "";
    tmpPublisher = NewzSocial.Article.publisherNames;

    if ( tmpContent.length > 200 ) {
        tmpContent = tmpContent.substr(0,200).concat('...');
    }

    if ( tmpPublisher == undefined || tmpPublisher == "" ) {
        var regex1 = /[0-9a-zA-Z]+[.][0-9a-zA-Z\-]+[.][0-9a-zA-Z]+/,
            regex2 = /[0-9a-zA-Z\-]+[.][0-9a-zA-Z]+/;
        if ( regex1.test( NewzSocial.Article.url ) ) tmpPublisher = regex1.exec( NewzSocial.Article.url );
        else if ( regex2.test( NewzSocial.Article.url ) ) tmpPublisher = regex2.exec( NewzSocial.Article.url );
        else tmpPublisher = "";
    }

    //publishedDate = convertUTCDateTolocalDate( NewzSocial.Article.publishedDate );
    //removes any damaged url links
    if( NewzSocial.Article.imgs.length != 0 ){
        var count = 0;
        for (i=0; i<NewzSocial.Article.imgs.length; i++){
            if(testUrlValidity(NewzSocial.Article.imgs[i])) tmp[count++] = NewzSocial.Article.imgs[i]; 
        }
        console.log("URL CHECKED!!!");
        NewzSocial.Article.imgs = tmp;
    }

    //checks if file is pdf file
    if ( NewzSocial.Article.url.indexOf(".pdf") != -1 ) {   
        isPDF = true;
        NewzSocial.Article.imgs = [];
    }

    if( NewzSocial.Article.url.indexOf(".jpg") != -1 || NewzSocial.Article.url.indexOf(".png") != -1 || NewzSocial.Article.url.indexOf(".gif") != -1 ) {
        isImg = true;
    }

    if( (NewzSocial.Article.imgs != undefined) && NewzSocial.Article.imgs[0] == "NaN" )
        NewzSocial.Article.imgs = [];

    var article = {
        id            : NewzSocial.Article.id,
        title         : HTMLDecode( NewzSocial.Article.title ), //special case
        content       : tmpContent,
        publisher     : tmpPublisher,
        publishedDate : publishedDate,
        image         : NewzSocial.Article.image?NewzSocial.Article.image:"",
        url           : NewzSocial.Article.url,
        imgs          : NewzSocial.Article.imgs, 
        PDF           : isPDF,
        isImg         : isImg,
        isPDForIMG    : (isImg||isPDF)
    };

    if(NewzSocial.Article.url.indexOf("nytimes.com") != -1){
        article.image = "http://static01.nyt.com/images/icons/t_logo_291_black.png";
        NewzSocial.Article.image = article.image;
    }

    if(NewzSocial.Article.url.indexOf("ft.com") != -1){
        article.image = "http://im.ft-static.com/m/img/masthead_main.jpg";
        NewzSocial.Article.image = article.image;
    }

    NewzSocial.Article.imageURL = NewzSocial.Article.image;

    return article;
}

// checks if the article web space contains the same article or still retains a previously indexed article
//function checkArticle() {}

// checks if image Url is valid
function testUrlValidity(url) {
    var encodedURL = encodeURIComponent(url);
    var isValid = false;

    $.ajax({
      url: "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%22" + encodedURL + "%22&format=json",
      type: "get",
      async: false,
      dataType: "json",
      success: function(data) {
        isValid = data.query.results != null;
      },
      error: function(){
        isValid = false;
      }
    });

    return isValid;
}

// calls update api to update articles
function UpdateArticle( uToken, article_id ){
    NewzSocial.API('UpdateArticle','content', null, uToken, [{'argument': 'articleId', 'parameter': article_id}, {'argument': 'title', 'parameter': NewzSocial.Article.title}, {'argument': 'description', 'parameter': NewzSocial.Article.description}, {'argument': 'imageUrl', 'parameter': NewzSocial.Article.image}], 0, callback);
    function callback( callNumber, response ) {
        functionResponse = response;
        if(response.status){
            console.log(response);
            console.log("Update Successful!");
        }else{
            console.log(response);
            console.log("Update Failed!")
        }
    }
}

// error function for forbidden access
function errorFunc() {
    NewzSocial.Article.image = "";
    NewzSocial.Article.imageURL = "";
    displayArticleInformation();
}

function convertUTCDateTolocalDate ( defaultPublishedDate ) {

    //a basic regex as we are expecting a standard date input
    var regexDate = /\d+[-]\d+[-]\d+/,
        regexTime = /\d+[:]\d+[:]\d+/,
        date = regexDate.exec( defaultPublishedDate ),
        time = regexTime.exec( defaultPublishedDate );
      
    var d = date[0].split("-"),
        t = time[0].split(":"),
        localPublishedDate = Date.UTC( d[0], d[1]-1, d[2], t[0], t[1], t[2] ), //milliseconds
        localDateString = new Date( localPublishedDate ),
        finalDate = {
            day    : localDateString.getDay(),
            date   : localDateString.getDate(),
            month  : localDateString.getMonth() + 1,
            year   : localDateString.getFullYear(),
            hour   : localDateString.getHours(),
            minute : localDateString.getMinutes(),
            second : localDateString.getSeconds()
        }

    switch ( finalDate.day ) {
        case 0:
            finalDate.day = "Sun";
        break;

        case 1:
            finalDate.day = "Mon";
        break;

        case 2:
            finalDate.day = "Tue";
        break;

        case 3:
            finalDate.day = "Wed";
        break;

        case 4:
            finalDate.day = "Thu";
        break;

        case 5:
            finalDate.day = "Fri";
        break;

        case 6:
            finalDate.day = "Sat";
        break;
    }

    var finalString = finalDate.day + ", " + finalDate.year + "-" + finalDate.month + "-";
        finalString += finalDate.date + ", " + finalDate.hour + ":" + finalDate.minute + ":"
        finalString += finalDate.second;
      
    return finalString;
}


//CLIP ACTION FUNCTION
function clip ( articleId ) {
    clearTimeout(timeOut);
    
    var uToken = $.cookie('newzsocial_uToken');
    NewzSocial.API('Clip', 's', null, uToken,[{'argument': 'articleId', 'parameter': articleId}], 0, callback);
    //we use class s here because we defined NewzSocial.api() s condition to use nscon/u/Clip?

    function callback ( callNumber, response ) {
        if( response.status ) {
            renderNewzSocialMessage("transition");
            displayMessage("Successful!","Article Clipped!",2,1500);
            setTimeout(function(){ renderNewzSocialAction(); },1500);
            timeOut = setTimeout(function(){ window.close(); },5000);
        }
        else{
            displayMessage("Error!","",1,1500);
            //shouldn't be here
        }
    }
}

//CURATE ACTION FUNCTION
function curate ( articleId ) {
    
    clearTimeout(timeOut);
    var uToken = $.cookie('newzsocial_uToken');

    getVirtualChannels ( uToken, callback );

    pdfImg = "";

    function callback ( virtualChannels, topCuratedIds, virtualChannelsIds ) {

        var topCuratedChannels = _.pick(virtualChannels,topCuratedIds),
            restChannels = _.pick(virtualChannels,virtualChannelsIds);
          
        topCuratedChannels = _.pairs(topCuratedChannels);
        restChannels = _.pairs(restChannels);
        
        topCuratedChannels = reorderRecentChannel(topCuratedIds, topCuratedChannels);

        var fullProperChannels = topCuratedChannels.concat(restChannels),
            article = displayArticleInformation();

        console.log("full proper channels: ", fullProperChannels);
      
        //window.resizeTo(680,520);
        if ( fullProperChannels.length != 0 ) {

            $('body').html(NewzSocial.Template.curate({
                article     : article,
                channels    : fullProperChannels,
                article_id  : articleId,
                display     : NewzSocial.User.display,
                pdfImg      : PDFimg
            }));

            $("#channels-select").select2({width:'100%'});
            $("#channels-select").focus();

            $(".curate-comment-tabs").click( function () {

                $(".curate-comment-tabs").removeClass("active");
                
                var htmlObj = $(this);
                htmlObj.addClass("active");

                switch ( htmlObj.attr("id") ) {
                    case "longCommentTab" :

                        $("#curateLongComment").show();
                        $("#curateShortComment, #shortCommentLimit").hide();

                    break;
                    case "shortCommentTab" :

                        if ( $("#curateLongComment").val().length && !$("#curateShortComment").val().length 
                            && ($("#curateShortComment").attr("data-state") == "firstTime") ) 
                        {
                            var longComment = $("#curateLongComment").val();

                            if ( longComment.length > 115 ) longComment = longComment.substr(0,112).concat("...");
                            $("#curateShortComment").val( longComment );
                            $("#curateShortComment").attr("data-state", "nonFirstTime");
                        }

                        $("#curateLongComment").hide();
                        $("#curateShortComment").show();
                        $("#shortCommentLimit").text( $("#curateShortComment").val().length + "/115").show();
                        
                    break;
                }

            });
            
            $("#curateShortComment").keyup( function () {
                $("#shortCommentLimit").text( $("#curateShortComment").val().length + "/115");
            })

            autoResize(680);
        }
        else{
            renderNewzSocialMessage("transition");
            window.resizeTo(450,280);
            $('#message').html("You are not the curator of any channels. Create a channel now on <a target='_blank' href='http://" + bookmarklet.appHost + ".newzsocial.com/newzlink/home'>" + bookmarklet.appHost + ".newzsocial.com!</a>");
            
            setTimeout(function(){
                renderNewzSocialAction();
            }, 4000);

            timeOut = setTimeout(function(){
                window.close();
            }, 8000);
        
        }
    }
}

function getVirtualChannels ( uToken, callback ) {
    var counter = 2,
        queryResponse = [],
        virtualChannels = {},
        virtualChannelsIds= [],
        topCuratedIds = [];

    NewzSocial.API('GetVirtualChannels', 'content', null, uToken, null , 0, collector);
    NewzSocial.API('GetUserProperty', 'user', null, uToken,[{'argument': 'property', 'parameter': 'TopCuratedIds'}],1,collector);
        
    function collector ( callNumber, response ) {
        counter--;
        queryResponse[callNumber] = response;
        
        if ( counter == 0 ) {

            for ( var i = 0; i < queryResponse[0].data.length; i++ ) {
                var channel = queryResponse[0].data[i];

                //Add Content Stream/Campaign Queue
                var parsedChannelName = channel.name;

                if (channel.searchContext==null || channel.searchContext==-1) parsedChannelName = parsedChannelName + " (Campaign Queue)";
                else if (channel.businessId && channel.searchContext && channel.searchContext!=-1) parsedChannelName = parsedChannelName + " (Content Stream)";

                virtualChannels[ channel.id ] = parsedChannelName;
                virtualChannelsIds.push( channel.id );
            }

            if ( queryResponse[1].data !== undefined ) {
                topCuratedIds = queryResponse[1].data.split(',');

                for ( var i = 0; i < topCuratedIds.length; i++ ) {
                    topCuratedIds[i] = parseInt(topCuratedIds[i]);
                }
        
                //due to the iPad team not fixing topCuratedIds user-property when deleting a channel
                //the below first check if there are any channel on the topCuratedIds list that are not on the virtualChannels
                //and removes them
                topCuratedIds = _.difference(topCuratedIds,_.difference(topCuratedIds,virtualChannelsIds));
                virtualChannelsIds = _.difference(virtualChannelsIds,topCuratedIds);
            }
            callback(virtualChannels,topCuratedIds,virtualChannelsIds);
        }
    }        
}

function reorderRecentChannel ( topCuratedIds, topCuratedChannels ) {
    var channels = topCuratedChannels,
        recentChannelOrder = [];

    for( var i = 0; i < topCuratedIds.length; i++ ){

        //channels is a object hash map, for/in loop is used here
        for( var j in channels ){
            if ( topCuratedIds[i] == channels[j][0] ) {
                recentChannelOrder.push(channels[j]);
                channels.splice(j,1);
                break;
            }
        }
    }
    return recentChannelOrder;
}

function curateArticle ( clip, articleId ) {
    var uToken = $.cookie('newzsocial_uToken'),
        channelIds = $('#channels-select').val(),
        userLongComment = encodeURIComponent( $('#curateLongComment').val() );
        userShortComment = encodeURIComponent( $('#curateShortComment').val() );

    if( NewzSocial.Article.image == "" || NewzSocial.Article.image === 'undefined' )
        NewzSocial.Article.image = imgPicked;

    if( displayArticleInformation().PDF || NewzSocial.Article.title == "" || NewzSocial.Article.title === 'undefined' ) {
        NewzSocial.Article.title = $('#title-of-pdf').val();
    }
    if( displayArticleInformation().PDF || NewzSocial.Article.description == "" || NewzSocial.Article.description === 'undefined' ) {
        NewzSocial.Article.description = $('#abstract').val();
    }
console.log("IMPT: " + NewzSocial.Article.update);
    if( !NewzSocial.Article.id ) {
        NewzSocial.CreateArticle();
    }else if ( NewzSocial.Article.id && NewzSocial.Article.update ){
        console.log("Updating");
        UpdateArticle(NewzSocial.User.uToken, NewzSocial.Article.id);
    }    

    if ( channelIds != null ) {
        callCurateAPI(uToken, articleId, channelIds, userLongComment, userShortComment);
          
        if ( clip ) {
            NewzSocial.API('Clip', 's', null, uToken,[{'argument': 'articleId', 'parameter': articleId}], 0, callback);
            //same reason we used the API at Clip function
            
            function callback ( callNumber, response ) {
                if(response.status) console.log('article was also clipped');
                else console.log('error: article was not clipped');
            }        
        }
    }
    else {
        displayMessage("Error!", "Please select at least one channel for curation.", 1, 1500);
    }
}

function callCurateAPI ( uToken, articleId, channelIds, userLongComment, userShortComment ) {
    var channelIds = channelIds.toString(),
        params = {
          channelIds : channelIds,
          articleId  : articleId
        };

    if ( userLongComment.length ) params.text = userLongComment;
    if ( userShortComment.length ) params.summary = userShortComment;

    NewzSocial.API_POST( bookmarklet.appHost,'nscon','c','Curate', params, 0, callback);
        
    function callback ( callNumber,response ) {
        if ( response.status ) { 
            addTopCuratedChannels(uToken, channelIds);
            renderNewzSocialMessage("transition");
            displayMessage("Successful!","Article Curated!",2,1500);
            
            setTimeout(function(){
                renderNewzSocialAction();
            },1500);

            timeOut = setTimeout(function(){
                window.close();
            },5000);
        }
    }
}

function addTopCuratedChannels ( uToken ,channelIds ) {
    var nonCuratedChannelIds;

    channelIds = channelIds.split(',');

    NewzSocial.API('GetUserProperty', 'user', null, uToken, [{'argument': 'property', 'parameter': 'TopCuratedIds'}], 1, callback);

    function callback ( callNumber, response ) {
        if ( response.data !== undefined ) {
            topCuratedIds = response.data.split(',');
            nonCuratedChannelIds = _.difference(topCuratedIds,channelIds);
            NewzSocial.API('SetUserProperty', 'user', null, uToken,[{'argument': 'property', 'parameter': 'TopCuratedIds'},{'argument': 'value', 'parameter': channelIds.concat(nonCuratedChannelIds).toString()}],1,function(callNumber,response){
                console.log('TopCuratedIds Updated!');
            });
        }
        else {
            NewzSocial.API('SetUserProperty', 'user', null, uToken,[{'argument': 'property', 'parameter': 'TopCuratedIds'},{'argument': 'value', 'parameter': channelIds.toString()}],1,function(callNumber,response){
                console.log('TopCuratedIds Updated!');
            });
        }
    }
}


//SHARE ACTION FUNCTION
function share ( clickType ) {
    clearTimeout(timeOut);

    pdfImg = "";

    var social_media_type, context, article,
        display = { //Information used for UI display
            userName       :  NewzSocial.User.display.userName,
            img            :  NewzSocial.User.display.img, 
            displayEmail   :  NewzSocial.User.display.email,
            SocialSettings :  (NewzSocial.User.profiles.smc.smcUserId === undefined)?false:true
        };

    //to check whether is the 'share' click or 'tab' click
    if ( clickType == 'default' ) social_media_type = "facebook";
    else social_media_type = clickType;

    console.log("show ", social_media_type, "tab clicked by ", clickType); //To check
    article = displayArticleInformation();

    switch ( social_media_type ) {
        
        case "email":    
            if ( NewzSocial.User.profiles.email ) { 
                context = {
                    isEmail   : true,                     
                    groups    : [],
                    friends   : NewzSocial.User.profiles.email.friends,
                    article   : article,
                    display   : display,
                    pdfImg    : PDFimg,
                    sharePipe : {                     
                        pipe  :  "email",
                        title :  "using Email"
                    },
                    profile   : {
                        name  : NewzSocial.User.profiles.email.detail.userName,
                        image : 'images/social-icons/email-profile-image.jpg'
                    },
                    shareTypes: []               
                }

                $('body').html(NewzSocial.Template.share(context));
                activateEmailEditor();
            }
            else {
                context = {
                    signUp  : true,
                    email   : true,
                    display : display,
                    title   : 'Email'
                }

                $('body').html(NewzSocial.Template.share(context));
                autoResize(680);
            }
            
            toggleTabEffect(social_media_type);
        break;

        case "facebook":
            if ( NewzSocial.User.profiles.facebook ) {
                context = {
                    groups     : NewzSocial.User.profiles.facebook.groups,
                    pages      : NewzSocial.User.profiles.facebook.pages,
                    article    : article,
                    display    : display,
                    pdfImg     : PDFimg,
                    sharePipe  : {
                        pipe   : "facebook",
                        title  : "on Facebook"
                    },
                    profile    : {
                        name   : NewzSocial.User.profiles.facebook.detail.userName
                    },
                    shareTypes : [{
                        type   : "timeline", 
                        label  : "Timeline"
                    }]        
                }

              if ( NewzSocial.User.profiles.facebook.groups ) context.shareTypes.push({type:"group",label:"Group"});
              if ( NewzSocial.User.profiles.facebook.pages ) context.shareTypes.push({type:"page",label:"Page"});

              if ( NewzSocial.User.profiles.facebook.detail.imageUrl ) context.profile.image =  NewzSocial.User.profiles.facebook.detail.imageUrl;
              else context.profile.image = "images/social-icons/email-profile-image.jpg";
            
              $('body').html(NewzSocial.Template.share(context));
              $("#share-type-select").select2({ minimumResultsForSearch: "-1" });

              //start gathering pipe data for dafult pipe
              if ( display.SocialSettings == true ) checkOaData(social_media_type);
            }
            else {
                context = {
                    signUp   : true,
                    facebook : true,
                    display  : display,
                    title    : "Facebook"
                }

              $('body').html(NewzSocial.Template.share(context));
              $.getScript('http://' + bookmarklet.nas + '.newzsocial.com/nas/js/nas.js', function(){ nasSetup(); });
            }

            autoResize(680);
            toggleTabEffect(social_media_type);
        break;

        case "linkedin":
            if ( NewzSocial.User.profiles.linkedin ) {
                context = {
                    groups     : NewzSocial.User.profiles.linkedin.groups,
                    pages      : NewzSocial.User.profiles.linkedin.pages,
                    article    : article,
                    display    : display,
                    pdfImg     : PDFimg,
                    sharePipe  : {
                        pipe   : "linkedin",
                        title  : "on LinkedIn"
                    }, 
                    profile    : {
                        name   : NewzSocial.User.profiles.linkedin.detail.userName
                    },
                    shareTypes :[
                        {type:"connections",label:"Connections"},
                        {type:"all",label:"Everyone"}
                    ]  
                }

                if ( NewzSocial.User.profiles.linkedin.groups ) context.shareTypes.push({type:"group",label:"Group"});
                if ( NewzSocial.User.profiles.linkedin.pages ) context.shareTypes.push({type:"page", label:"Page"});

                if(NewzSocial.User.profiles.linkedin.detail.imageUrl) context.profile.image =  NewzSocial.User.profiles.linkedin.detail.imageUrl;
                else context.profile.image = "images/social-icons/email-profile-image.jpg";
               
                $('body').html(NewzSocial.Template.share(context));
                $("#share-type-select").select2({ minimumResultsForSearch: "-1" });

                //start gathering pipe data for dafult pipe
                if ( display.SocialSettings == true ) checkOaData(social_media_type);
            }
            else {
                context = {
                    signUp   : true,
                    linkedin : true,
                    display  : display,
                    title    : "LinkedIn"
                }

                $('body').html(NewzSocial.Template.share(context));
                $.getScript('http://' + bookmarklet.nas + '.newzsocial.com/nas/js/nas.js', function(){ nasSetup(); });
            }

            autoResize(680);
            toggleTabEffect(social_media_type);
        break;

        case "twitter":
            if ( NewzSocial.User.profiles.twitter ) {
                context = {  
                    groups     : [],
                    article    : article,
                    display    : display,
                    pdfImg     : PDFimg,
                    sharePipe  : {
                        pipe   : "twitter",
                        title  : "on Twitter"
                    },                
                    profile    : {
                        name   : NewzSocial.User.profiles.twitter.detail.userName
                    },
                    shareTypes : []  
                }

                if(NewzSocial.User.profiles.twitter.detail.imageUrl) context.profile.image =  NewzSocial.User.profiles.twitter.detail.imageUrl;
                else context.profile.image = "images/social-icons/email-profile-image.jpg";
            

                $('body').html(NewzSocial.Template.share(context));
                //start gathering pipe data for dafult pipe
                if(display.SocialSettings == true) checkOaData(social_media_type);
            }
            else {
                context = {
                    signUp  : true,
                    twitter : true,
                    display : display,
                    title   : "Twitter"
                }

              $('body').html(NewzSocial.Template.share(context));
              $.getScript('http://' + bookmarklet.nas + '.newzsocial.com/nas/js/nas.js', function(){ nasSetup(); });
            }
            
            autoResize(680);
            toggleTabEffect(social_media_type);
        break;
    }
}

//functions to toggle the different tab and present different UI effect
function toggleTabEffect ( type ) {
    //toggle the Navbar Effect
    $('#tab-facebook, #tab-linkedin, #tab-twitter, #tab-email')
        .removeClass('active')                    // deactivate all tabs
        .find('img.tab-icon-on').hide()               // hide all active icons
        .end().find('img.tab-icon-off').show();           // show all inactive icons
        
    switch ( type ) {
        case "facebook"  :
            $('#tab-facebook').addClass('active').find('img.tab-icon-off').hide().end().find('img.tab-icon-on').show();
            $('#shareButton').show();
            $("#title > a").addClass("facebook-style");
        break;
          
        case "linkedin" :
            $('#tab-linkedin').addClass('active').find('img.tab-icon-off').hide().end().find('img.tab-icon-on').show();
            $('#shareButton').show();
            $("#title > a").addClass("linkedin-style");
        break;
          
        case "twitter"  :
            $('#tab-twitter').addClass('active').find('img.tab-icon-off').hide().end().find('img.tab-icon-on').show();
            $("#shareButton-twt").show();
            $("#publisher, #content, .article-info").hide();
            $("#share-user-comment").focus().val('"' + constructTweet( NewzSocial.Article.title ) + '"');
            $("#title > a").addClass("twitter-style").html('<a target="_blank" href="'+NewzSocial.Article.url+'"><i>'+NewzSocial.Article.url+"</i></a> via #NewzSocial");
            countWords( type );
        break;
          
        case "email"    :
            $('#tab-email').addClass('active').find('img.tab-icon-off').hide().end().find('img.tab-icon-on').show();
            $('#pipeIt').hide();
            $('#shareButton-email').show();
            $("#emailInput").select2({
                width:'100%',
                // closeOnSelect: false,
                tags:function(){
                    var emailFriendsAsTags = [];

                    for ( var i = 0; i < NewzSocial.User.profiles.email.sendListFriend.length; i++ ) {
                        var friend = NewzSocial.User.profiles.email.sendListFriend[i];
                        emailFriendsAsTags.push({id:friend.email,text:friend.displayUserName + " (" + friend.email + ")"});
                    }

                    var allEmails = emailFriendsAsTags.concat(NewzSocial.User.profiles.email.sendListNonFriend);

                    return allEmails;
                },
            
                tokenSeparators:[" ",",",";"],
                createSearchChoice:function(term){
                    if(!NewzSocial.Utility.isEmail(term)) return null;
                    return {id:term,text:term};
                },
                
                formatNoMatches: function(item){
                    return "No matches found. <span class='text-success'>You can also enter email addresses manually.</span>";
                }
            });

            $("#title").addClass("email-style");
            $("#emailInput").focus();
        break;
    }
}

function changePlatformType ( pipe ) {
    switch ( pipe ) {
        case "facebook":
            switch( $('#share-type-select').val() ) {
                case "timeline":
                    $(".share-group, .share-page").hide();
                    autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);
                break;

                case "group":
                    $("#share-group-select").select2({allowClear: true});
                    $(".share-page").hide();
                    $(".share-group").show();
                    $("#share-group-select").focus();
                    autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);
                break;
                    
                case "page":
                    $("#share-page-select").select2({allowClear: true });
                    $(".share-group").hide();
                    $(".share-page").show();
                    $("#share-page-select").focus();
                    autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);
                break;
            }
        break;

        case "linkedin":
            switch( $('#share-type-select').val() ) {
                case "connections":
                    $(".share-group, .share-page").hide();
                   autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);                
                break;

                case "all":
                    $(".share-group, .share-page").hide();
                    autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);
                break;

                case "group":
                    $("#share-group-select").select2({allowClear: true});
                    $(".share-page").hide();
                    $(".share-group").show();
                    $("#share-group-select").focus();
                    autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);
                break;

                case "page":
                    $("#share-page-select").select2({allowClear: true });
                    $(".share-group").hide();
                    $(".share-page").show();
                    $("#share-page-select").focus();
                    autoResize(680);
                    if(NewzSocial.User.profiles.smc.smcUserId != undefined) checkOaData(pipe);
                break;
              
            }
        break;
          
        case "twitter":
            checkOaData(pipe);
        break;
    }
}


function activateEmailEditor () {
    var article = displayArticleInformation();
    $('#emailSubject').val(article.title);

    tinyMCE.init({
        selector: "textarea#email-editor",
        width: 630,
        height: 250,
        menubar : false,
        statusbar : false,
        setup: function(ed) {
            ed.on('init', function(e) {
                setTimeout( function () {
                    autoResize(680);
                }, 300);
            });
        },
        plugins: [
            "advlist autolink link lists preview",
            "directionality paste textcolor"
        ],
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | preview | forecolor | link ",
    }); 
}

function constructTweet ( title ) {
    if ( title.length > 98 ) title = title.substr(0,47).concat("...");
    return title;
}

function countWords ( sharePipe ) {
    if ( sharePipe == "twitter" ) {
        $('#share-user-comment').attr("maxlength",100);

        var limit = 100,
            count = $('#share-user-comment').val().length;

        $('#share-comment-count').html(count + "/" + limit);
        if ( count == limit ) displayMessage("Error!", "You have reached the character limit", 1, 1500);
        
    }
}

function shareArticle ( pipe ) {
    var uToken = $.cookie('newzsocial_uToken');
    
    if( NewzSocial.Article.image == "" || NewzSocial.Article.image === undefined || NewzSocial.Article.image == "null" ) {
        NewzSocial.Article.image = imgPicked;
    }
console.log("image: " + imgPicked +  " vs " + NewzSocial.Article.image);
    if( displayArticleInformation().isPDForIMG || NewzSocial.Article.title == "" || NewzSocial.Article.title === undefined || NewzSocial.Article.title == "null" ) {
        NewzSocial.Article.title = $('#title-of-pdf').val();
    }

    if( displayArticleInformation().isPDForIMG || NewzSocial.Article.description == "" || NewzSocial.Article.description === undefined || NewzSocial.Article.description == "null" ) {
        NewzSocial.Article.description = $('#abstract').val();
    }

    if( !NewzSocial.Article.id ) {
        NewzSocial.CreateArticle();
    }else if ( NewzSocial.Article.id && NewzSocial.Article.update ){
        UpdateArticle(NewzSocial.User.uToken, NewzSocial.Article.id);
        console.log("Updating");
    }
        
    switch ( pipe ) {
        case "email":
            if ( !$('#emailInput').val() ) {
                displayMessage("Error!", "Please select a friend to share this article with.", 1, 1500);
            }
            else{
                var postData            = {},
                    selectedFullFriends = $('#emailInput').val().toLowerCase().split(','),
                    newzsocialFriends   = [];

                for ( var i = 0; i < NewzSocial.User.profiles.email.friends.length; i++ ) {
                    var friend = NewzSocial.User.profiles.email.friends[i];
                    newzsocialFriends.push(friend.oaProfile.email);
                }

                var selectedNonNewzsocialEmails = _.difference(selectedFullFriends,newzsocialFriends);
                    selectedNewzSocialEmails    = _.difference(selectedFullFriends,selectedNonNewzsocialEmails);

                //Save used emails in history
                saveEmailNonFriend(uToken,selectedNonNewzsocialEmails);

                if ( $('#emailMe').is(':checked') ) selectedNonNewzsocialEmails.push(NewzSocial.User.profiles.email.detail.email);

                postData["oaType"] = 1;
                postData["contentType"] =  4;
                postData["contentId"] = NewzSocial.Article.id;
                postData["oaUserIdsOrEmails"] = selectedNewzSocialEmails.toString();
                postData["nonUsers"] =  selectedNonNewzsocialEmails.toString();
                postData["userComment"] =  tinyMCE.activeEditor.getContent();
                postData["title"] =  NewzSocial.Article.title;
                postData["imageUrl"] =  NewzSocial.Article.image;
                postData["publisher"] =  NewzSocial.Article.publisherNames;
                postData["publishedDate"] =  NewzSocial.Article.publishedDate;
                postData["content"] =  NewzSocial.Article.description;
                postData["url"] =  NewzSocial.Info.browser.url;
                if ( $('#emailSubject').val() ) postData["userSubject"] = $('#emailSubject').val();
                console.log(postData);
                callShareAPI( postData, "default" );
            }
        break;
          
        case "facebook":
            switch ( $('#share-type-select').val() ) {
                case "timeline":
                    var postData = {};
                    
                    postData = {'oaType': 2,'contentType': 4,'contentId': NewzSocial.Article.id, 'uToken':uToken, 'aToken':NewzSocial.Application.aToken};
                    
                    if ( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                    else postData.userComment = "";
                    
                    callShareAPI( postData, "all" );
    
                break;

                case "group":
                    if ( !$('#share-group-select').val() ) displayMessage("Error!", "Please select a group to share this article with.", 1, 1500);
                    else {
                        var postData = {},
                            groupIds = $('#share-group-select').val().toString();
                        
                        postData = {'oaType': 2,'contentType': 4,'contentId': NewzSocial.Article.id, 'oaGroupIds':groupIds, 'uToken':uToken, 'aToken':NewzSocial.Application.aToken};
                        if ( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                        else postData.userComment = "";
                        console.log(postData);
                        callShareAPI( postData, "group" );
                    }
                break;
              
                case "page":
                    if ( !$('#share-page-select').val() ) displayMessage("Error!", "Please select a page to share this article on.", 1, 1500);
                    else {
                        var postData = {},
                            pageIds = $('#share-page-select').val().toString();
                  
                        postData = {'oaType': 2,'contentType': 4,'contentId': NewzSocial.Article.id, 'oaPageIds':pageIds, 'uToken':uToken, 'aToken':NewzSocial.Application.aToken};
                        if( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                        else postData.userComment = "";
    
                        callShareAPI( postData, "page" );
                    }
                break;
            }
        break;
        
        case "linkedin":
            switch($('#share-type-select').val()){
                case "connections":
                    var postData = {};
                    
                    postData = {'oaType': 4,'contentType': 4,'contentId': NewzSocial.Article.id, 'uToken':uToken,'aToken':NewzSocial.Application.aToken};
                    if ( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                    else postData.userComment = "";

                    callShareAPI( postData, "all" );

                break;
              
                case "group":
                    if ( !$('#share-group-select').val() ) displayMessage("Error!", "Please select a group to share this article with.", 1, 1500);
                    else {
                        var postData = {},
                            groupIds = $('#share-group-select').val().toString();
                        
                        postData = {'oaType': 4,'contentType': 4,'contentId': NewzSocial.Article.id, 'oaGroupIds':groupIds, 'uToken':uToken,'aToken':NewzSocial.Application.aToken};
                  
                        if ( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                        else postData.userComment = "";
        
                        callShareAPI( postData, "group" );
                    }
                break;
              
                case "page":
                    if ( !$('#share-page-select').val() ) displayMessage("Error!", "Please select a page to share this article on.", 1, 1500);
                    else {
                        var postData = {},
                            pageIds = $('#share-page-select').val().toString();
                  
                        postData = {'oaType': 4,'contentType': 4,'contentId': NewzSocial.Article.id, 'oaPageIds':pageIds, 'uToken':uToken, 'aToken':NewzSocial.Application.aToken};
                        if ( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                        else postData.userComment = "";
                      
                        callShareAPI( postData, "page" );
                    }
                break;
              
                case "all" :
                    var postData = {};
                    
                    postData = {'oaType': 4,'contentType': 4,'contentId': NewzSocial.Article.id, 'uToken':uToken, 'shareAll' : true, 'aToken':NewzSocial.Application.aToken};
                    if ( $('#share-user-comment').val() ) postData.userComment = $('#share-user-comment').val();
                    else postData.userComment = "";
            
                    callShareAPI( postData, "all" );
                break;
            }
        break;
        
        case "twitter":
            var postData = {},
                userTweet = $('#share-user-comment').val();

            userTweet = userTweet + "\n-" + NewzSocial.Article.url + " via #NewzSocial" 
            
            postData = {'oaType': 3,'contentType': 4,'contentId': NewzSocial.Article.id, 'uToken':uToken, 'aToken':NewzSocial.Application.aToken};
            if ( $('#share-user-comment').val() ) postData.userComment = userTweet;    
            else postData.userComment = "";
            
            callShareAPI( postData, "all" );
        break;
    }
}

function callShareAPI ( postData, type ) {

    renderNewzSocialMessage("sharing");
    console.log("postData : ", postData);
    
    var api; 
    switch ( type ) {
        case "all" :
            api = "ShareWithAll"
        break;

        case "group" :
            api = "ShareWithGroup"
        break;

        case "page" :
            api = "ShareWithPage"
        break;

        default :
            api = "ShareCustomArticleOnMedia"
    }   
    if(type == "default")
        NewzSocial.API_POST2( bookmarklet.appHost, "pnscon", "u", api, postData, 0, callback);
    else NewzSocial.API_POST( bookmarklet.appHost, "nscon", "u", api, postData, 0, callback);
   
    function callback ( callNumber, response ) {
        if ( response.status ) {
            renderNewzSocialMessage("transition");
            displayMessage("Successful!","Article Shared!",2,1500);
           /* setTimeout(function(){
                renderNewzSocialAction();
            },1500);

            timeOut = setTimeout(function(){
                window.close();
            },5000); */
        }
        else {
            displayMessage("Error!","Share Article Failed!",1,1500);
            console.log(response);
           /* setTimeout(function(){
                renderNewzSocialAction();
            },1500);

            timeOut = setTimeout(function(){
                window.close();
            },5000); */
        }
    }
}

//Save the past emails into history for email sharing function only
function saveEmailNonFriend ( uToken, new_emails ) {
    var old_emails;

    NewzSocial.API('GetUserProperty','user', null, uToken, [{'argument': 'property','parameter': 'savedEmails'}],0,function(callNumber, response){
    
        if ( response.data !== undefined ) {
            old_emails = response.data.split(',');

            NewzSocial.API('SetUserProperty', 'user', null, uToken, [{'argument': 'property', 'parameter': 'savedEmails'}, {'argument': 'value', 'parameter': old_emails.concat(_.difference(new_emails, old_emails)).toString()}],0,function(callNumber, response){
                console.log("Save Emails Successful!");
            });

        }
        else {
            NewzSocial.API('SetUserProperty','user', null, uToken, [{'argument': 'property', 'parameter':'savedEmails'}, {'argument': 'value', 'parameter': new_emails.toString()}],0,function(callNumber, response){
                console.log("First Time Save Emails Successful!");
            });
        }
    });
}

// New Functions for buttons in handlebars
var count = 0;
var imgPicked = new String();

function ChangeImageSrcNext(){
    var imgs = NewzSocial.Article.imgs;
    var img = document.getElementById('myImage'); 
    var index = (++count % imgs.length) + imgs.length;
    img.src = decodeURIComponent(imgs[index%imgs.length]);
    console.log("CLICKED!");
    imgPicked = imgs[index%imgs.length];
}

function ChangeImageSrcPrev(){
    var imgs = NewzSocial.Article.imgs;
    var img = document.getElementById('myImage'); 
    var index = (--count % imgs.length) + imgs.length;
    img.src = decodeURIComponent(imgs[index%imgs.length]);
    console.log("CLICKED!");
    imgPicked = imgs[index%imgs.length];
}

// back function
function back(){
    renderNewzSocialAction();
}

//preview images
function previewImg(){
    var img = document.getElementById('PDF');
    img.src = $('#image-url').val();
    console.log("image: " + img.src);
    imgPicked = img.src;
    PDFimg = img.src;
}